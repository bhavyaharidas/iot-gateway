package neu.bhavyaharidas.connecteddevices.common;

import com.google.gson.Gson;

/**
 * @author Bhavya Haridas
 * 
 * Class represents utility methods to convert sensor data and actuator data objects into json and vice versa
 */
public class DataUtil {

	/**
	 * Returns json string corresponding to the given sensordata object
	 * @param sensorData
	 * @return
	 */
	public String toJsonFromSensorData(SensorData sensorData) {
		String jsonData = null;
		if (sensorData != null) {
		Gson gson = new Gson();
		jsonData = gson.toJson(sensorData);
		}
		return jsonData;
	}
	
	/**
	 * Returns the sensordata object corresponsding to the given json string
	 * @param jsonString
	 * @return
	 */
	public SensorData toSensorDataFromJson(String jsonString) {
		SensorData sensorData = null;
		if (jsonString != null && jsonString.trim().length() > 0) {
		Gson gson = new Gson();
		sensorData = gson.fromJson(jsonString, SensorData.class);
		}
		return sensorData;
	}
	
	public boolean writeSensorDataToFile(SensorData sensorData) {
		return true;
	}
	
	/**
	 * Returns json string corresponding to the given actuatorData object
	 * @param actuatorData
	 * @return
	 */
	public String toJsonFromActuatorData(ActuatorData actuatorData) {
		String jsonData = null;
		if (actuatorData != null) {
		Gson gson = new Gson();
		jsonData = gson.toJson(actuatorData);
		}
		return jsonData;
	}
	
	public String toUbidotsJsonFromActuatorData(ActuatorData actuatorData) {
		String jsonData = "{\"" + actuatorData.getName() + "\": {\"value\": " + actuatorData.getValue() + "}}";
		return jsonData;
	}
	
	/**
	 * Returns the actuatordata object corresponsding to the given json string
	 * @param jsonString
	 * @return
	 */
	public ActuatorData toActuatorDataFromJson(String jsonString) {
		ActuatorData actuatorData = null;
		if (jsonString != null && jsonString.trim().length() > 0) {
		Gson gson = new Gson();
		actuatorData = gson.fromJson(jsonString, ActuatorData.class);
		}
		return actuatorData;
	}
	
	/**
	 * 
	 * @param jsonString
	 * @return
	 */
	public PerformanceData toPerformanceDataFromJson(String jsonString) {
		PerformanceData data = null;
		if (jsonString != null && jsonString.trim().length() > 0) {
			Gson gson = new Gson();
			data = gson.fromJson(jsonString, PerformanceData.class);
		}
		return data;
	}
	
	public boolean writeActuatorDataToFile(ActuatorData actuatorData) {
		return true;
	}

	public String toUbidotsJsonFromSensorData(float sensorData) {
		String jsonData = "{\"pressure\": {\"value\": " + sensorData + "}}";
		return jsonData;
	}
}
