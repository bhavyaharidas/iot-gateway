package neu.bhavyaharidas.connecteddevices.common;


import java.util.logging.Logger;

import neu.bhavyaharidas.connecteddevices.labs.module05.GatewayDataManager;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPubSub;

/**
 * Class Represents listener object for monitoring for incoiming sensor data
 * @author bhavy
 *
 */
public class SensorDataListener {

	static PersistenceUtil persistenceUtil = new PersistenceUtil();
	
	/**
	 * Returns true if a the flag corresponding to temperature or humidity sensing is set by the gateway
	 * @return
	 */
	public boolean onMessage() {
		String temp_flag = persistenceUtil.getTempFlag();
		String hum_flag = persistenceUtil.getHumFlag();
		
		while((temp_flag != null && temp_flag.equals("actuator")) && (hum_flag != null && hum_flag.equals("actuator"))) {
			temp_flag = persistenceUtil.getTempFlag();
			hum_flag = persistenceUtil.getHumFlag();
		}
		return true;
	}

}