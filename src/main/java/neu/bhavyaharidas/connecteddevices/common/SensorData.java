package neu.bhavyaharidas.connecteddevices.common;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Class represents collection of data related to sensor
 * @author Bhavya Haridas
 *
 */
public class SensorData implements Serializable
{
	//private fields
	private String timeStamp = null;
	private String name = "Not Set";
	private float curValue = 0.0f;
	private float avgValue = 0.0f;
	private float minValue = 0.0f;
	private float maxValue = 0.0f;
	private float totValue = 0.0f;
	private int sampleCount = 0;
	
	//Getters and setters
	
	public String getTimeStamp() {
		return timeStamp;
	}
	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public float getCurValue() {
		return curValue;
	}
	public void setCurValue(float curValue) {
		this.curValue = curValue;
	}
	public float getAvgValue() {
		return avgValue;
	}
	public void setAvgValue(float avgValue) {
		this.avgValue = avgValue;
	}
	public float getMinValue() {
		return minValue;
	}
	public void setMinValue(float minValue) {
		this.minValue = minValue;
	}
	public float getMaxValue() {
		return maxValue;
	}
	public void setMaxValue(float maxValue) {
		this.maxValue = maxValue;
	}
	public float getTotValue() {
		return totValue;
	}
	public void setTotValue(float totValue) {
		this.totValue = totValue;
	}
	public int getSampleCount() {
		return sampleCount;
	}
	public void setSampleCount(int sampleCount) {
		this.sampleCount = sampleCount;
	}
	
	//Constructor
	public SensorData(String name)
	{
		super();
		this.name = name;
		updateTimeStamp();
	}
	
	/**
	 * Adds the given val to the currValue field if between 0 and 30
	 * Recalculates average, min, max and count.
	 * Prints new data
	 * @param val
	 */
	public void addValue(float val)
	{
		if(val < 0 || val > 30) {
			System.out.println("Value out of range");
			return;
		}
		updateTimeStamp(); 
		++this.sampleCount;
		this.curValue = val;
		this.totValue += val;
		if (this.curValue < this.minValue || this.sampleCount == 1) {
			this.minValue = this.curValue;
		}
		if (this.curValue > this.maxValue) {
			this.maxValue = this.curValue;
		}
		if (this.totValue != 0 && this.sampleCount > 0) {
			this.avgValue = this.totValue / this.sampleCount;
		}
		//System.out.println(getData());
	}
	//Returns string representing current object values
	public String getData() {
		String data = this.getName() + ":\n\tTime:" + this.timeStamp + "\n\tCurrent:" + this.curValue + "\n\tAverage:" + this.avgValue +
				"\n\tSamples:" + this.sampleCount + "\n\tMin:" + this.minValue + "\n\tMax:" + this.maxValue;
		return data;
	}
	//Updates current time
	private void updateTimeStamp() {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		LocalDateTime now = LocalDateTime.now();
		this.timeStamp = dtf.format(now);  
		
	}
	/**
	 * @return byte array representing current object values
	 */
	public byte[] getBytes() {
		return getData().getBytes();
	}
}