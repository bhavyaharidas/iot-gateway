package neu.bhavyaharidas.connecteddevices.common;

import java.io.Serializable;

public class PerformanceData implements Serializable{
	//private fields
	private String timeStamp = null;
	private float memory = 0.0f;
	private float cpu = 0.0f;
	public String getTimeStamp() {
		return timeStamp;
	}
	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}
	public float getMemory() {
		return memory;
	}
	public void setMemory(float memory) {
		this.memory = memory;
	}
	public float getCpu() {
		return cpu;
	}
	public void setCpu(float cpu) {
		this.cpu = cpu;
	}
}
