package neu.bhavyaharidas.connecteddevices.common;

import java.util.logging.Logger;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

/**
 * Class representsClass represents utility methods to store and retrieve data to/from redis database
 * @author Bhavya Haridas
 *
 */
public class PersistenceUtil {
	
	private static final Logger _Logger = Logger.getLogger(PersistenceUtil.class.getSimpleName());
	public static final String CHANNEL_NAME = "commonChannel";
	
	ConfigUtil configUtil = ConfigUtil.getInstance("../iot-gateway/config/ConnectedDevicesConfig.props");
	String redis_host= (configUtil.getValue(ConfigConst.REDIS, ConfigConst.REDIS_HOST));
	Integer redis_port= Integer.valueOf((configUtil.getValue(ConfigConst.REDIS, ConfigConst.REDIS_PORT)));
	String redis_auth= (configUtil.getValue(ConfigConst.REDIS, ConfigConst.REDIS_AUTH));
	
	Jedis jedis = new Jedis();	//Jedis instance
	JedisPool jedispool = new JedisPool (redis_host ,redis_port);
	
	DataUtil dataUtil;
	SensorData sensorData;
	static int sampleCount = 0;
	static int actuatorCount = 0;
	static int flag =0;
	
	//Consructor
	public PersistenceUtil() {
		this.dataUtil = new DataUtil();
		this.jedispool = jedispool;
		this.jedis =jedispool.getResource();
		this.jedis.auth(redis_auth);
		System.out.println("Connection Successful");
	}

	/**
	 * Writes the sensordata object to redis db as json string
	 * @param sensorData
	 * @return
	 */
	public boolean writeSensorDataToDbms(SensorData sensorData) {
		String jsonString = this.dataUtil.toJsonFromSensorData(sensorData);
		_Logger.info(jsonString);
		return true;
	}
	
	/**
	 * Writes the actuatorData object to redis db as json string
	 * @param actuatorData
	 * @return
	 */
	public boolean writeActuatorDataToDbms(ActuatorData actuatorData) {
		String key="actuate_" + actuatorCount;
		String actuatorJson = this.dataUtil.toJsonFromActuatorData(actuatorData);
		jedis.set(key, actuatorJson);
		_Logger.info("Wrote actuator data -\n" + actuatorJson);
		if(actuatorData.getName().equals("Temperature"))
			this.setTempFlag();
		else
			this.setHumFlag();

		actuatorCount++;
		return true;
	}
	
	/**
	 * Reads the sensorData json string from redis
	 * @return
	 */
	public SensorData readSensorDataFromDbms() {
		String key="sample_" + sampleCount;
		String jsonString=jedis.get(key);
		if(jsonString != null) {
			sensorData = this.dataUtil.toSensorDataFromJson(jsonString);
			_Logger.info("New sensor readings:\n" + sensorData.getData());
			sampleCount++;
			return sensorData;
		}
		return null;
	}
	
	/*
	public void registerSensorDataDbmsListener(SensorDataListener sensorDataListener) {
		final JedisPoolConfig poolConfig = new JedisPoolConfig();
    	final JedisPool jedisPool = new JedisPool(poolConfig, "localhost", 6379, 0);
    	final Jedis subscriberJedis = jedisPool.getResource();
    	final SensorDataListener subscriber = new SensorDataListener();

    	new Thread(new Runnable() {
        @Override
        public void run() {
            try {
            	_Logger.info("Subscribing to \"commonChannel\". This thread will be blocked.");
                subscriberJedis.subscribe(subscriber, CHANNEL_NAME);
                _Logger.info("Subscription ended.");
            } catch (Exception e) {
            	_Logger.info("Subscribing failed.");
            }
        }
    }).start();
	}
	*/
	
	public void registerActuatorDataDbmsListener(ActuatorDataListener actuatorDataListener) {
		
		actuatorDataListener.onMessage();
	}
	
	// Methods to get and set flag

	public void setTempFlag() {
		String key = "temp_flag";
		String value = "actuator";
		jedis.set(key ,value);
	}
	
	public void setHumFlag() {
		String key = "hum_flag";
		String value = "actuator";
		jedis.set(key ,value);
	}
	
	public String getTempFlag() {
		String key = "temp_flag";
		return jedis.get(key);
	}
	
	public String getHumFlag() {
		String key = "hum_flag";
		return jedis.get(key);
	}
	
}
