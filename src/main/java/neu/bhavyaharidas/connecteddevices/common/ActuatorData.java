package neu.bhavyaharidas.connecteddevices.common;

/**
 * Class represents actuator data
 * @author Bhavya Haridas
 *
 */
public class ActuatorData {
	
	private String name;
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

	public float getValue() {
		return value;
	}

	public void setValue(float value) {
		this.value = value;
	}

	private String command;
	private float value;
	
	public ActuatorData(String name, String command, float value) {
		this.name = name;
		this.command = command;
		this.value = value;
	}
}
