package neu.bhavyaharidas.connecteddevices.common;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Bhavya Haridas
 *
 * Singleton class represents Configuration utilities to store and retrieve config.prop values
 */
public class ConfigUtil {
	
	private static String configFileName;
	private static ConfigUtil configUtil = null;
	
	//private static Properties properties;
	
	private Pattern  _section  = Pattern.compile( "\\s*\\[([^]]*)\\]\\s*" );
	private Pattern  _keyValue = Pattern.compile( "\\s*([^=]*)=(.*)" );
	private static Map<String, Map<String, String>> _entries = new HashMap<>();
	
	//constructor
	public ConfigUtil(String configFileName) {
		this.configFileName = configFileName;
		//this.properties = new Properties();
		if(loadConfig()) {
			System.out.println("Successfully loaded config values");
		}
		 else{
			 System.out.println("Failed to load config values");
		 }
	}
	
	/**
	 * Returns the singleton instance of class
	 * @param fileName
	 * @return
	 */
	public static ConfigUtil getInstance(String fileName) {
		 if (configUtil == null) 
			 configUtil = new ConfigUtil(fileName); 
		 return configUtil;
	}
	
	/**
	 * @param section - section in prop file
	 * @param key - key in prop file
	 * @return - Returns the value against given key and section. Empty string if key, section or both doesn't exists
	 */
	public String getValue(String section, String key) {
		if(_entries.containsKey(section)) {
			String value = _entries.get(section).get(key);
			return value == null ? "" : value;
		}
		return "";
	}

	/**
	 * Loads config values from prop file into _entries map using Matcher and Pattern
	 * @return boolean represtning completion
	 */
	public boolean loadConfig() {
		try(BufferedReader br = new BufferedReader(new FileReader(this.configFileName))) {
	         String line;
	         String section = null;
	         try {
				while(( line = br.readLine()) != null ) {
				    Matcher m = _section.matcher( line );
				    if( m.matches()) {
				       section = m.group( 1 ).trim();
				    }
				    else if( section != null ) {
				       m = _keyValue.matcher( line );
				       if( m.matches()) {
				          String key   = m.group( 1 ).trim();
				          String value = m.group( 2 ).trim();
				          Map< String, String > kv = _entries.get( section );
				          if( kv == null ) {
				             _entries.put( section, kv = new HashMap<>());   
				          }
				          kv.put( key, value );
				       }
				    }
				 }
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	      } catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return true;
	}
	
	/**
	 * @return boolean representing whether class instance in created
	 */
	public boolean hasConfigData() {
		return !(configUtil==null);
	}
}    
         



