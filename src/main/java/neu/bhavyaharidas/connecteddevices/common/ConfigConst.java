package neu.bhavyaharidas.connecteddevices.common;

/**
 * Class Represents constant config keys
 * 
 * @author Bhavya Haridas
 *
 */
public class ConfigConst {

	public static final String SMTP_CLOUD_SECTION = "smtp.cloud";
	public static final String PORT_KEY = "port";
	public static final String SMTP_PROP_HOST_KEY = "mail.smtp.host";
	public static final String HOST_KEY = "host";
	public static final String SMTP_PROP_AUTH_KEY = "mail.smtp.auth";
	public static final String SMTP_PROP_PORT_KEY = "mail.smtp.port";
	public static final String ENABLE_AUTH_KEY = "enableAuth";
	public static final String SMTP_PROP_ENABLE_TLS_KEY = "mail.smtp.starttls.enable";
	public static final String ENABLE_CRYPT_KEY = "enableCrypt";
	public static final String FROM_ADDRESS_KEY = "fromAddr";
	public static final String TO_ADDRESS_KEY = "toAddr";
	public static final String AUTH_TOKEN = "authToken";

	public static final String DEVICE_SECTION = "device";
	public static final String NOMINAL_DEVICE_TEMP = "nominalTemp";
	public static final String NOMINAL_DEVICE_HUMID = "15";

	// Redis
	public static final String REDIS = "redis";
	public static final String REDIS_HOST = "redis_host";
	public static final String REDIS_PORT = "redis_port";
	public static final String REDIS_AUTH = "redis_auth";

	// Mqtt
	public static final String MQTT_SECTION = "mqtt.cloud";
	public static final String DEFAULT_MQTT_PROTOCOL = "tcp";
	public static final String DEFAULT_MQTT_SERVER = "mqtt.eclipse.org";
	public static final int DEFAULT_MQTT_PORT = 1883;
	public static final int DEFAULT_QOS_LEVEL = 1;

	public static final String MQTT_HOST = "host";
	public static final String MQTT_PORT = "port";
	public static final String SECURE_MQTT_PORT = "securePort";
	public static final String SECURE_MQTT_PROTOCOL = "secureProtocol";

	// CoAP
	public static final String COAP_SECTION = "coap.cloud";
	public static final String DEFAULT_COAP_PORT = "port";
	public static final String SECURE_COAP_PROTOCOL = "coaps";
	public static final String DEFAULT_COAP_PROTOCOL = "coap";
	public static final String SECURE_COAP_PORT = "securePort";
	public static final String DEFAULT_COAP_SERVER = "host";

	// Ubidots
	public static final String UBIDOTS_SECTION = "ubidots.cloud";
	public static final String DEFAULT_UBIDOTS_SERVER = "host";
	public static final String CERT_FILE_PATH = "certFile";
	public static final String UBIDOTS_AUTH_TOKEN = "authToken";
	public static final String TEMP_TOPIC = "tempTopic";
	public static final String UBIDOTS_PORT = "port";
	public static final String UBIDOTS_API_KEY = "apiKey";
	public static final String TEMP_VARIABLE_ID = "tempVariableId";
	public static final String HUMID_VARIABLE_ID = "humidVariableId";
	public static final String HUMID_ACTUATOR_ID = "humidActuatorId";
	public static final String PRESSURE_VARIABLE_ID = "pressureVariableId";
	public static final String SENSOR_CPU_VARIABLE_ID = "sensorCpuVariableId";
	public static final String SESNOR_MEM_VARIABLE_ID = "sensorMemoryVariableId";
	public static final String GATEWAY_CPU_VARIABLE_ID = "gatewayCpuVariableId";
	public static final String GATEWAY_MEM_ACTUATOR_ID = "gatewayMemoryVariableId";
	
	
	//AWS IoT Core
	public static final String AWS_SECTION = "aws.iot";
	public static final String AWS_ENDPOINT = "endpoint";
	public static final String TEMP_CLIENT_ID = "tempClientId";
	public static final String AWS_CERT_FILE_PATH = "certFile";
	public static final String PVT_KEY_FILE = "pvtKey";

}
