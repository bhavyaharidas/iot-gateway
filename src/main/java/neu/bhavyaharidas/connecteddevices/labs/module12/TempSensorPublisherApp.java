package neu.bhavyaharidas.connecteddevices.labs.module12;

import java.util.Random;
import java.util.logging.Logger;

import com.amazonaws.services.iot.client.AWSIotException;

import neu.bhavyaharidas.connecteddevices.common.ConfigConst;
import neu.bhavyaharidas.connecteddevices.common.ConfigUtil;
import neu.bhavyaharidas.connecteddevices.labs.module12.MqttClientConnector;

public class TempSensorPublisherApp {

	private static final Logger logger = Logger.getLogger(TempSensorPublisherApp.class.getName());
	
	private static TempSensorPublisherApp app;
	private static MqttClientConnector mqttConnector;
	private AwsMqttClient awsConnector;

	private void setMqttClient() {
		this.mqttConnector.connect();
		this.mqttConnector.subscribeToTopic("SensorData");
	}
	
	/**
	 * MqttPubClientTestApp Constructor
	 */
	public TempSensorPublisherApp() {
		//Initialize aws client
		try {
			awsConnector = new AwsMqttClient();
		} catch (AWSIotException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		mqttConnector = new MqttClientConnector(awsConnector);
		this.setMqttClient();
	}

	/**
	 * method to connect mqqt client to broker and publish the message
	 * @param topicName: name of the MQTT session topic in string
	 */
	public void start() {
		while (true) {
					
					//this.awsConnector.subscribe("actuator/temp/data");
					try {
						Thread.sleep(10000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
		}

	/**
	 * Ramdomly generates floating point number between 
	 * min and max value to emulate sensor value
	 * @param min: minimum value to be generated
	 * @param max: maximum value to be generated
	 */
	public float generateRandomvalue(float min, float max) {
		Random r = new Random();
		float random = min + r.nextFloat() * (max - min);
		logger.info("\n\n Temperature Reading : " + Float.toString(random) + "\n");
		return random;
	}

	/**
	 * Main function of MQTT publisher class
	 */
	public static void main(String[] args) {
		app = new TempSensorPublisherApp();
		//app.start();
		try {
			Thread.sleep(100000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
