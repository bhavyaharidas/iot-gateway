package neu.bhavyaharidas.connecteddevices.labs.module12;

import com.amazonaws.services.iot.client.AWSIotMessage;
import com.amazonaws.services.iot.client.AWSIotQos;
import com.amazonaws.services.iot.client.AWSIotTopic;

public class AwsTopic extends AWSIotTopic {
    public AwsTopic(String topic, AWSIotQos qos) {
        super(topic, qos);
    }

    @Override
    public void onMessage(AWSIotMessage message) {
        System.out.print("Received " + message.getStringPayload());
    }
}
