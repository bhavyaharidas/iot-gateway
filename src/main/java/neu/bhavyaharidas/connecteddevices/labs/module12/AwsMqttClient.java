package neu.bhavyaharidas.connecteddevices.labs.module12;
import org.joda.time.DateTime;

import com.amazonaws.services.iot.client.AWSIotException;
import com.amazonaws.services.iot.client.AWSIotMqttClient;
import com.amazonaws.services.iot.client.AWSIotQos;
import com.amazonaws.services.iot.client.sample.sampleUtil.SampleUtil;
import com.amazonaws.services.iot.client.sample.sampleUtil.SampleUtil.KeyStorePasswordPair;

import neu.bhavyaharidas.connecteddevices.common.ConfigConst;
import neu.bhavyaharidas.connecteddevices.common.ConfigUtil;
import neu.bhavyaharidas.connecteddevices.common.SensorData;

public class AwsMqttClient {
	
	private ConfigUtil config = ConfigUtil.getInstance("../iot-gateway/config/ConnectedDevicesConfig.props");
	
	private String clientEndpoint = config.getValue(ConfigConst.AWS_SECTION, ConfigConst.AWS_ENDPOINT);    // replace <prefix> and <region> with your own
	private String clientId = config.getValue(ConfigConst.AWS_SECTION, ConfigConst.TEMP_CLIENT_ID);                             // replace with your own client ID. Use unique client IDs for concurrent connections.
	private String certificateFile = config.getValue(ConfigConst.AWS_SECTION, ConfigConst.CERT_FILE_PATH);                      // X.509 based certificate file
	private String privateKeyFile = config.getValue(ConfigConst.AWS_SECTION, ConfigConst.PVT_KEY_FILE);
	
	AWSIotMqttClient client;
	
	public AwsMqttClient() throws AWSIotException {
		KeyStorePasswordPair pair = SampleUtil.getKeyStorePasswordPair(certificateFile, privateKeyFile);
		System.out.println("Loaded key pair");
		client = new AWSIotMqttClient(clientEndpoint, clientId, pair.keyStore, pair.keyPassword);
		System.out.println("Initialized client");
		client.connect();
		System.out.println("Connected to client");
	}
	
	public void publishSensorData(String sensorData) {
		String topic = "sensor/data";
		//String payload = "{ \"timestamp\": \"" + sensorData.getTimeStamp() + "\",\"temperature\": \"" + sensorData.getCurValue() + "\" }" ;
		this.publish(topic, sensorData);
	}
	
	public void publish(String topic, String payload) {

		try {
			System.out.println("Publishing value " + payload + " to topic " + topic);
			client.publish(topic, AWSIotQos.QOS0, payload);
		} catch (AWSIotException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void subscribe(String topicName) {
		//String topicName = "my/own/topic";
		AWSIotQos qos = AWSIotQos.QOS0;

		AwsTopic topic = new AwsTopic(topicName, qos);
		try {
			client.subscribe(topic);
			System.out.print("Subscribed to topic" + topic);
		} catch (AWSIotException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
