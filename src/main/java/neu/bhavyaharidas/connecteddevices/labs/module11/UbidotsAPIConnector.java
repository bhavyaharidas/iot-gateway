package neu.bhavyaharidas.connecteddevices.labs.module11;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.eclipse.paho.client.mqttv3.MqttMessage;
import com.ubidots.*;

import neu.bhavyaharidas.connecteddevices.common.ActuatorData;
import neu.bhavyaharidas.connecteddevices.common.ConfigConst;
import neu.bhavyaharidas.connecteddevices.common.ConfigUtil;

/**
 * @author Bhavya Haridas
 *
 * UbidotsAPIConnector Class represents ubidots api connector
 */
public class UbidotsAPIConnector {
	
	private static final Logger logger = Logger.getLogger("gatewayApp");
	private static ApiClient apiConnector;
	private static DataSource dataSource;
	private ConfigUtil config = ConfigUtil.getInstance("../iot-gateway/config/ConnectedDevicesConfig.props");
	
	//private fields
	private String host = config.getValue(ConfigConst.UBIDOTS_SECTION, ConfigConst.DEFAULT_UBIDOTS_SERVER);
	private String pemFileName = config.getValue(ConfigConst.UBIDOTS_SECTION, ConfigConst.CERT_FILE_PATH);
	private String authToken = config.getValue(ConfigConst.UBIDOTS_SECTION, ConfigConst.UBIDOTS_AUTH_TOKEN);
	private String apiKey = config.getValue(ConfigConst.UBIDOTS_SECTION, ConfigConst.UBIDOTS_API_KEY);
	private String tempVariableId = config.getValue(ConfigConst.UBIDOTS_SECTION, ConfigConst.TEMP_VARIABLE_ID);
	private String humidVariableId = config.getValue(ConfigConst.UBIDOTS_SECTION, ConfigConst.HUMID_VARIABLE_ID);
	private String pressureVariableId = config.getValue(ConfigConst.UBIDOTS_SECTION, ConfigConst.PRESSURE_VARIABLE_ID);
	private String humidActuatorId = config.getValue(ConfigConst.UBIDOTS_SECTION, ConfigConst.HUMID_ACTUATOR_ID);
	
	private String sensorCpuVariableId = config.getValue(ConfigConst.UBIDOTS_SECTION, ConfigConst.SENSOR_CPU_VARIABLE_ID);
	private String sensorMemoryVariableId = config.getValue(ConfigConst.UBIDOTS_SECTION, ConfigConst.SESNOR_MEM_VARIABLE_ID);
	private String gatewayCpuVariableId = config.getValue(ConfigConst.UBIDOTS_SECTION, ConfigConst.GATEWAY_CPU_VARIABLE_ID);
	private String gatewayMemoryVariableId = config.getValue(ConfigConst.UBIDOTS_SECTION, ConfigConst.GATEWAY_MEM_ACTUATOR_ID);
	
	//static variables
	private static Variable tempVariable;
	private static Variable humidVariable;
	private static Variable humidActuator;
	private static Variable pressureVariable;
	
	private static Variable sensorCpuVariable;
	private static Variable sensorMemoryVariable;
	private static Variable gatewayCpuVariable;
	private static Variable gatewayMemoryVariable;
	
	//Constructor
	public UbidotsAPIConnector() {
		this.connect();
		this.createVariables();

	}
	
	//Connect to the api client
	private void connect()
	{
		apiConnector = new ApiClient(apiKey); // this is a Ubidots class
		// need to call 'fromToken()' on ApiClient before updating base URL,
		// as it will create the underlying ServerBridge if not already created
		//apiConnector.fromToken(apiKey);
		//if (host != null && host.length() > 0) {
			//apiConnector.setBaseUrl(host);
		//}
	}
	
	/**
	 * Creates instances of various variables
	 */
	public void createVariables() {
		//tempVariable = apiConnector.getVariable(tempVariableId);
		humidVariable = apiConnector.getVariable(humidVariableId);
		humidActuator = apiConnector.getVariable(humidActuatorId);
		pressureVariable = apiConnector.getVariable(pressureVariableId);
		sensorCpuVariable = apiConnector.getVariable(sensorCpuVariableId);
		sensorMemoryVariable = apiConnector.getVariable(sensorMemoryVariableId);
		gatewayCpuVariable = apiConnector.getVariable(gatewayCpuVariableId);
		gatewayMemoryVariable = apiConnector.getVariable(gatewayMemoryVariableId);
	}
	
	/**
	 * publishing message
	 * 
	 * @param: topic --> topic to publish message to
	 * @param: qosLevel --> setting the qos level
	 * @param: payload --. the message to be sent
	 
	public boolean publishActuatorData(ActuatorData actuatorData) {
		return mqttConnector.publishActuatorData(actuatorData);
	}
	*/
	
	/**
	 * Posts humidity value
	 * @param value
	 */
	public void postHumidValue(float value) {
		logger.info("Sending humidity value : " + value + "  to API");
		this.humidVariable.saveValue(value);
	}
	
	/**
	 * Posts pressure value
	 * @param value
	 */
	public void postPressureValue(float value) {
		logger.info("Sending pressure value : " + value + "  to API");
		this.pressureVariable.saveValue(value);
	}
	
	/**
	 * Posts sensor cpu value
	 * @param value
	 */
	public void postSensorCpu(float value) {
		logger.info("Sending Sensor CPU value : " + value + "  to API");
		this.sensorCpuVariable.saveValue(value);
	}
	
	/**
	 * Posts sensor memory value
	 * @param value
	 */
	public void postSensorMemory(float value) {
		logger.info("Sending Sensor Memory value : " + value + "  to API");
		this.sensorMemoryVariable.saveValue(value);
	}
	
	/**
	 * Posts gateway cpu value
	 * @param value
	 */
	public void postGatewayCpu(float value) {
		logger.info("Sending Gateway CPU value : " + value + "  to API");
		this.gatewayCpuVariable.saveValue(value);
	}
	
	/**
	 * Posts gateway memory value
	 * @param value
	 */
	public void postGatewayMemory(float value) {	
		logger.info("Sending Gateway Memory value : " + value + "  to API");
		this.gatewayMemoryVariable.saveValue(value);
	}
	
	/**
	 * returns temp value
	 * @param value
	 */
	public float getTempValue() {
		Value[] values = this.tempVariable.getValues();
		return (float) values[0].getValue();
	}
	
	/**
	 * Returns humidity actuator value
	 * @param value
	 */
	public float getHumidActuatorValue() {
		Value[] values = this.humidActuator.getValues();
		return (float) values[0].getValue();
	}

}
