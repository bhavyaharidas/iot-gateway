package neu.bhavyaharidas.connecteddevices.labs.module11;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import neu.bhavyaharidas.connecteddevices.common.ConfigConst;
import neu.bhavyaharidas.connecteddevices.common.ConfigUtil;
import neu.bhavyaharidas.connecteddevices.labs.module11.MqttClientConnector;
/**
 * @author Bhavya Haridas
 *
 * Main App class - initializes instances of various connectors and performance measurement tasks.
 */
public class PressureSensorGatewayApp {

	private static final Logger logger = getLogger();
	private ConfigUtil config = ConfigUtil.getInstance("../iot-gateway/config/ConnectedDevicesConfig.props");
	
	//Ubidots topics
	public static final String UBIDOTS_VARIABLE_LABEL = "/pressure_actuator";
	public static final String UBIDOTS_DEVICE_LABEL = "/barometer";
	public static final String UBIDOTS_TOPIC_DEFAULT = "/v1.6/devices" + UBIDOTS_DEVICE_LABEL + UBIDOTS_VARIABLE_LABEL+"/lv";
	
	//Ubidots security properties
	private String host = config.getValue(ConfigConst.UBIDOTS_SECTION, ConfigConst.DEFAULT_UBIDOTS_SERVER);
	private String pemFileName = config.getValue(ConfigConst.UBIDOTS_SECTION, ConfigConst.CERT_FILE_PATH);
	private String authToken = config.getValue(ConfigConst.UBIDOTS_SECTION, ConfigConst.UBIDOTS_AUTH_TOKEN);
	
	//Static instances
	private static PressureSensorGatewayApp app;
	private static SystemPerformanceTask performanceTask;
	private static MqttClientConnector mqttConnector;
	private static MqttClientConnector secureMqttConnector;
	private UbidotsAPIConnector ubidotsAPIConnector;

	/**
	 * PressureSensorGatewayApp Constructor
	 */
	public PressureSensorGatewayApp() {
		//To post values to ubidots
		this.ubidotsAPIConnector = new UbidotsAPIConnector();
		
		//To subscribe to local mqtt topics
		mqttConnector = new MqttClientConnector(this.ubidotsAPIConnector);
		this.mqttConnector.connect();
		this.mqttConnector.subscribeToTopic("SensorData", 1);
		this.mqttConnector.subscribeToTopic("System", 1);
		
		//To subscribe to Ubidots mqtt topics
		secureMqttConnector = new MqttClientConnector(host, authToken, pemFileName, mqttConnector);
		secureMqttConnector.connect();
		secureMqttConnector.subscribeToTopic(UBIDOTS_TOPIC_DEFAULT,1);
		
		// Start measuring performance parameters
		performanceTask = new SystemPerformanceTask(this.ubidotsAPIConnector, 20);
		this.performanceTask.startPolling();
	}

	private static Logger getLogger() {
		Logger logger = Logger.getLogger("gatewayApp");
		FileHandler fh;  

	    try {  
	        // This block configure the logger with handler and formatter  
	        fh = new FileHandler("C:\\Coursework\\CSYE6530-ConnectedDevices\\eclipse\\iot-gateway\\src\\main\\java\\neu\\bhavyaharidas\\connecteddevices\\labs\\module11\\gateway.log");  
	        logger.addHandler(fh);
	        SimpleFormatter formatter = new SimpleFormatter();  
	        fh.setFormatter(formatter);  

	    } catch (SecurityException e) {  
	        e.printStackTrace();  
	    } catch (IOException e) {  
	        e.printStackTrace();  
	    }
		return logger;  
	}

	/**
	 * Main function
	 */
	public static void main(String[] args) {
		app = new PressureSensorGatewayApp();
	}
}
