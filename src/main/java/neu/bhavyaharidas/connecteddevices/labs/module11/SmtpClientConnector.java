package neu.bhavyaharidas.connecteddevices.labs.module11;

import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import neu.bhavyaharidas.connecteddevices.common.ConfigConst;
import neu.bhavyaharidas.connecteddevices.common.ConfigUtil;

/**
 * 
 * @author Bhavya Haridas
 * Class represents Connector for SMPT
 *
 */
public class SmtpClientConnector {

	//private variables
	private static final Logger _Logger = Logger.getLogger("gatewayApp");
	private boolean _isConnected;
	private Session smtpSession;
	private ConfigUtil config = ConfigUtil.getInstance("../iot-gateway/config/ConnectedDevicesConfig.props");
	
	/**
	 * Description - Initializes authentication and session instances for SMPT
	 * Sets _isConnected to true after successful completion
	 */
	public void connect()
	{
		if (!_isConnected) {
			_Logger.info("Initializing SMTP gateway...");
			Properties props = new Properties();
			//Getting config values from configutil
			String port = config.getValue(ConfigConst.SMTP_CLOUD_SECTION, ConfigConst.PORT_KEY);
			String host = config.getValue(ConfigConst.SMTP_CLOUD_SECTION, ConfigConst.HOST_KEY);
			String enableAuth = config.getValue(ConfigConst.SMTP_CLOUD_SECTION, ConfigConst.ENABLE_AUTH_KEY);
			String enableCrypt = config.getValue(ConfigConst.SMTP_CLOUD_SECTION, ConfigConst.ENABLE_CRYPT_KEY);
			final String fromAddrStr = config.getValue(ConfigConst.SMTP_CLOUD_SECTION, ConfigConst.FROM_ADDRESS_KEY);
			final String password = config.getValue(ConfigConst.SMTP_CLOUD_SECTION, ConfigConst.AUTH_TOKEN);
			
			props.put(ConfigConst.SMTP_PROP_HOST_KEY, host);
			props.put(ConfigConst.SMTP_PROP_PORT_KEY, port);
			props.put(ConfigConst.SMTP_PROP_AUTH_KEY, enableAuth);
			props.put(ConfigConst.SMTP_PROP_ENABLE_TLS_KEY, enableCrypt);
			
			_Logger.info(props.toString());
			
			Authenticator auth = new Authenticator() {
				//override the getPasswordAuthentication method
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(fromAddrStr, password);
				}
			}; 
			this.smtpSession = Session.getInstance(props, auth);
			this._isConnected = true;
		} else {
			_Logger.info("SMTP gateway connection already initialized.");
		}
	}
	
	/**
	 * Description - Creates internet address objects for from and to addresses and sends the smptp message over email
	 * @param subject - SUbject of email
	 * @param payload - Body of email
	 * @return - boolean indicating result of action
	 */
	public boolean publishMessage(String subject, byte[] payload)
	{
		if (! this._isConnected) {
			connect(); 
		}
		boolean success = false;
		String fromAddrStr =config.getValue(ConfigConst.SMTP_CLOUD_SECTION, ConfigConst.FROM_ADDRESS_KEY);
		try {
			Message smtpMsg = new MimeMessage(this.smtpSession);
			InternetAddress fromAddr = new InternetAddress(fromAddrStr);
			InternetAddress[] toAddr =
					InternetAddress.parse(config.getValue(ConfigConst.SMTP_CLOUD_SECTION, ConfigConst.TO_ADDRESS_KEY));
			smtpMsg.setFrom(fromAddr);
			smtpMsg.setRecipients(Message.RecipientType.TO, toAddr);
			smtpMsg.setSubject(subject);
			String body = new String(payload);
			smtpMsg.setText(body);
			Transport.send(smtpMsg);
			success = true;
		} catch (Exception e) {
			_Logger.log(Level.WARNING, "Failed to send SMTP message from address: " + fromAddrStr, e);
		}
		return success;
	}

}
