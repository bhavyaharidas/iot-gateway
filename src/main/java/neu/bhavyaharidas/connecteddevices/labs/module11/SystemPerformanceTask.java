package neu.bhavyaharidas.connecteddevices.labs.module11;

import java.lang.management.ManagementFactory;
import java.lang.management.MemoryMXBean;
import java.lang.management.MemoryUsage;
import java.util.concurrent.TimeUnit;
import com.sun.management.OperatingSystemMXBean;
import neu.bhavyaharidas.connecteddevices.common.PerformanceData;

/**
 * @author Bhavya Haridas
 *
 * SystemPerformanceTask class represents a loop that regularly polls for system performance measures
 */

public class SystemPerformanceTask {
	
	//Ubidots connector
	private UbidotsAPIConnector apiConnector;
	private int _pollCycle;
	
	public SystemPerformanceTask(UbidotsAPIConnector apiConnector, int _pollCycle) {
		this.apiConnector = apiConnector;
		this._pollCycle = _pollCycle;
	}
	
	/**
	 * Runs in a loop to poll performance measures
	 */
	public void startPolling() {
		
		while(true) {
			PerformanceData data = new PerformanceData();
			data.setMemory(this.getMemory());
			data.setCpu(this.getCpu());
			this.apiConnector.postGatewayCpu(data.getCpu());
			this.apiConnector.postGatewayMemory(data.getMemory());
			try {
				TimeUnit.SECONDS.sleep(_pollCycle);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Returns mxbean cpu percentage
	 * @return
	 */
	public float getCpu() {
		OperatingSystemMXBean bean = (OperatingSystemMXBean) ManagementFactory.getOperatingSystemMXBean();
		float res = (float) bean.getSystemCpuLoad() * 100;
		return res;
	}
	
	/**
	 * Returns mxbean memory percentage
	 * @return
	 */
	public float getMemory() {
		MemoryMXBean memBean = ManagementFactory.getMemoryMXBean();
		MemoryUsage heap = memBean.getHeapMemoryUsage();
		MemoryUsage nonHeap = memBean.getNonHeapMemoryUsage();
		MemoryUsage memUsage = ManagementFactory.getMemoryMXBean().getHeapMemoryUsage();
		
		double heapUtil = ((double)heap.getUsed() / heap.getMax()) * 100d;
		double nonHeapUtil = ((double)nonHeap.getUsed() / nonHeap.getMax()) * 100;
		
		if(heapUtil < 0.0d) {
			heapUtil = 0.0d;
		}
		if(nonHeapUtil < 0.0d) {
			nonHeapUtil = 0.0d;
		}
		
		//_Logger.info("\tHeap Max:    " + heap.getMax() + "\tHeap Used:    " + heap.getUsed());
		//_Logger.info("\tNon-Heap Max:    " + nonHeap.getMax() + "\tNon-Heap Used:    " + nonHeap.getUsed());
		return (float)heapUtil;
	}
}
