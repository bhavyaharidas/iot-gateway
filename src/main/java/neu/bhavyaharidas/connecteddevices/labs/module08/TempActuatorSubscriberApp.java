package neu.bhavyaharidas.connecteddevices.labs.module08;

import neu.bhavyaharidas.connecteddevices.common.ConfigConst;
import neu.bhavyaharidas.connecteddevices.common.ConfigUtil;

public class TempActuatorSubscriberApp {
	/**
	 * Receive message using MQTT protocol (subscribes to Mqtt Topic) 
	 * 
	 * @var subscriberApp: instance variable for TempActuatorSubscriberApp
	 * @var mqttClient: instance variable for MqqtClientConnector
	 * @var host: Broker address for Mqtt
	 * @var pemfileName: file location of the Ubidots pem file
	 * @var UBIDOTS_VARIABLE_LABEL: string constant for ubidots variable name
	 * @var UBIDOTS_VARIABLE_LABEL: String constant for ubidots Device name
	 * @var UBIDOTS_TOPIC_DEFAULT: Topic name to subscribe/publish
	 */
	
	private ConfigUtil config = ConfigUtil.getInstance("../iot-gateway/config/ConnectedDevicesConfig.props");
	private int nominalTemp =  Integer.parseInt(this.config.getValue(ConfigConst.DEVICE_SECTION, ConfigConst.NOMINAL_DEVICE_TEMP));
	private int nominalHumid = 15;
	static int iterations;
	private int previousMessageId = 0;
	
	private String host = config.getValue(ConfigConst.UBIDOTS_SECTION, ConfigConst.DEFAULT_UBIDOTS_SERVER);
	private String pemFileName = config.getValue(ConfigConst.UBIDOTS_SECTION, ConfigConst.CERT_FILE_PATH);
	private String authToken = config.getValue(ConfigConst.UBIDOTS_SECTION, ConfigConst.UBIDOTS_AUTH_TOKEN);
	
	private static TempActuatorSubscriberApp subscriberApp;


	private static MqttClientConnector secureMqttConnector;
	public static final String UBIDOTS_VARIABLE_LABEL = "/tempactuator";
	public static final String UBIDOTS_DEVICE_LABEL = "/sensepi";
	public static final String UBIDOTS_TOPIC_DEFAULT = "/v1.6/devices" + UBIDOTS_DEVICE_LABEL + UBIDOTS_VARIABLE_LABEL+"/lv";

	/**
	 * MqttSubClientTestApp constructor
	 */
	public TempActuatorSubscriberApp() {
		super();
	}

	/**
	 * method to connect mqqt client to broker and publish the message
	 * @param topicName: name of the MQTT session topic in string
	 * 
	 */
	public void start(String topicName) {
		try {
			secureMqttConnector = new MqttClientConnector(host, authToken, pemFileName);
			//secureMqttConnector.setCallback(new MqttClientConnector());
			secureMqttConnector.connect();
			
			secureMqttConnector.subscribeToTopic(topicName,1);
				Thread.sleep(60000);
			}
		 catch (Exception e) {
			e.printStackTrace();
			secureMqttConnector.disconnect();
		}
	}
	

	/**
	 * Main function of TempActuatorSubscriber class
	 */
	public static void main(String[] args) {

		subscriberApp = new TempActuatorSubscriberApp();
		try {
			subscriberApp.start(UBIDOTS_TOPIC_DEFAULT);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
}
