package neu.bhavyaharidas.connecteddevices.labs.module08;

import java.util.Random;
import java.util.logging.Logger;

import neu.bhavyaharidas.connecteddevices.common.ConfigConst;
import neu.bhavyaharidas.connecteddevices.common.ConfigUtil;

public class TempSensorPublisherApp {

	private static final Logger logger = Logger.getLogger(TempSensorPublisherApp.class.getName());
	private static TempSensorPublisherApp app;
	private ConfigUtil config = ConfigUtil.getInstance("../iot-gateway/config/ConnectedDevicesConfig.props");
	private int nominalTemp =  Integer.parseInt(this.config.getValue(ConfigConst.DEVICE_SECTION, ConfigConst.NOMINAL_DEVICE_TEMP));
	private int nominalHumid = 15;
	static int iterations;
	private int previousMessageId = 0;
	
	private String host = config.getValue(ConfigConst.UBIDOTS_SECTION, ConfigConst.DEFAULT_UBIDOTS_SERVER);
	private String pemFileName = config.getValue(ConfigConst.UBIDOTS_SECTION, ConfigConst.CERT_FILE_PATH);
	private String authToken = config.getValue(ConfigConst.UBIDOTS_SECTION, ConfigConst.UBIDOTS_AUTH_TOKEN);
	
	private static TempActuatorSubscriberApp subscriberApp;


	private static MqttClientConnector secureMqttConnector;
	private static UbidotsAPIConnector ubidotsAPIConnector;
	public static final String UBIDOTS_VARIABLE_LABEL = "/tempsensor";
	public static final String UBIDOTS_DEVICE_LABEL = "/SensorPi";
	public static final String UBIDOTS_TOPIC_DEFAULT = "/v1.6/devices" + UBIDOTS_DEVICE_LABEL + UBIDOTS_VARIABLE_LABEL;


	/**
	 * MqttPubClientTestApp Constructor
	 */
	public TempSensorPublisherApp() {
		super();
	}

	/**
	 * method to connect mqqt client to broker and publish the message
	 * @param topicName: name of the MQTT session topic in string
	 */
	public void start(String topicName) {
		
			try {
				ubidotsAPIConnector = new UbidotsAPIConnector();
				secureMqttConnector = new MqttClientConnector(host, authToken, pemFileName);
				secureMqttConnector.connect();
				while (true) {
					secureMqttConnector.publishTempSensorData(generateRandomvalue(0.0f, 40.0f));
					this.ubidotsAPIConnector.postHumidValue(generateRandomvalue(0.0f, 40.0f));
					Thread.sleep(2000);
					float humidActuatorValue = this.ubidotsAPIConnector.getHumidActuatorValue();
					System.out.println("Received humidity actuation value from API - " + humidActuatorValue);
					Thread.sleep(60000);
				}
			} catch (Exception e) {				
				secureMqttConnector.disconnect();
				e.printStackTrace();
			}
		}

	/**
	 * Ramdomly generates floating point number between 
	 * min and max value to emulate sensor value
	 * @param min: minimum value to be generated
	 * @param max: maximum value to be generated
	 */
	public float generateRandomvalue(float min, float max) {
		Random r = new Random();
		float random = min + r.nextFloat() * (max - min);
		logger.info("\n\n Temperature Reading : " + Float.toString(random) + "\n");
		return random;
	}

	/**
	 * Main function of MQTT publisher class
	 */
	public static void main(String[] args) {
		app = new TempSensorPublisherApp();
		
			app.start(UBIDOTS_TOPIC_DEFAULT);
	}
}
