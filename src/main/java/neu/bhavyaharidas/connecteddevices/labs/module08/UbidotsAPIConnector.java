package neu.bhavyaharidas.connecteddevices.labs.module08;

import java.util.logging.Level;

import org.eclipse.paho.client.mqttv3.MqttMessage;
import com.ubidots.*;

import neu.bhavyaharidas.connecteddevices.common.ActuatorData;
import neu.bhavyaharidas.connecteddevices.common.ConfigConst;
import neu.bhavyaharidas.connecteddevices.common.ConfigUtil;

public class UbidotsAPIConnector {
	
	private static ApiClient apiConnector;
	private static DataSource dataSource;
	private ConfigUtil config = ConfigUtil.getInstance("../iot-gateway/config/ConnectedDevicesConfig.props");
	
	private String host = config.getValue(ConfigConst.UBIDOTS_SECTION, ConfigConst.DEFAULT_UBIDOTS_SERVER);
	private String pemFileName = config.getValue(ConfigConst.UBIDOTS_SECTION, ConfigConst.CERT_FILE_PATH);
	private String authToken = config.getValue(ConfigConst.UBIDOTS_SECTION, ConfigConst.UBIDOTS_AUTH_TOKEN);
	private String apiKey = config.getValue(ConfigConst.UBIDOTS_SECTION, ConfigConst.UBIDOTS_API_KEY);
	private String tempVariableId = config.getValue(ConfigConst.UBIDOTS_SECTION, ConfigConst.TEMP_VARIABLE_ID);
	private String humidVariableId = config.getValue(ConfigConst.UBIDOTS_SECTION, ConfigConst.HUMID_VARIABLE_ID);
	private String humidActuatorId = config.getValue(ConfigConst.UBIDOTS_SECTION, ConfigConst.HUMID_ACTUATOR_ID);
	
	private static Variable tempVariable;
	private static Variable humidVariable;
	private static Variable humidActuator;
	
	public UbidotsAPIConnector() {
		this.connect();
		this.createVariables();
		//mqttConnector = new MqttClientConnector(host, authToken, pemFileName);
		//mqttConnector.connect();
	}
	
	private void connect()
	{
		apiConnector = new ApiClient(apiKey); // this is a Ubidots class
		// need to call 'fromToken()' on ApiClient before updating base URL,
		// as it will create the underlying ServerBridge if not already created
		//apiConnector.fromToken(apiKey);
		//if (host != null && host.length() > 0) {
			//apiConnector.setBaseUrl(host);
		//}
	}
	
	public void createVariables() {
		//tempVariable = apiConnector.getVariable(tempVariableId);
		humidVariable = apiConnector.getVariable(humidVariableId);
		humidActuator = apiConnector.getVariable(humidActuatorId);
	}
	
	/**
	 * publishing message
	 * 
	 * @param: topic --> topic to publish message to
	 * @param: qosLevel --> setting the qos level
	 * @param: payload --. the message to be sent
	 
	public boolean publishActuatorData(ActuatorData actuatorData) {
		return mqttConnector.publishActuatorData(actuatorData);
	}
	*/
	
	public void postHumidValue(float value) {
		System.out.println("Sending humidity value : " + value + "  to API");
		this.humidVariable.saveValue(value);
	}
	
	public float getTempValue() {
		Value[] values = this.tempVariable.getValues();
		return (float) values[0].getValue();
	}
	
	public float getHumidActuatorValue() {
		Value[] values = this.humidActuator.getValues();
		return (float) values[0].getValue();
	}

}
