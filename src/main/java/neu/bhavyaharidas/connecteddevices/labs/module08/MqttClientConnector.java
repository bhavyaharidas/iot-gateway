package neu.bhavyaharidas.connecteddevices.labs.module08;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

import neu.bhavyaharidas.connecteddevices.common.ActuatorData;
import neu.bhavyaharidas.connecteddevices.common.ConfigConst;
import neu.bhavyaharidas.connecteddevices.common.ConfigUtil;
import neu.bhavyaharidas.connecteddevices.common.DataUtil;
import neu.bhavyaharidas.connecteddevices.common.SensorData;

public class MqttClientConnector implements MqttCallback {
	private static final Logger logger = Logger.getLogger(MqttClientConnector.class.getName());
	private ConfigUtil config = ConfigUtil.getInstance("../iot-gateway/config/ConnectedDevicesConfig.props");
	private String protocol = ConfigConst.DEFAULT_MQTT_PROTOCOL;
	private String host = ConfigConst.DEFAULT_MQTT_SERVER;
	private int port = ConfigConst.DEFAULT_MQTT_PORT;

	private String brokerAddr;
	private MqttClient mqttClient;
	private DataUtil dataUtil;
	private MqttMessage mqttMessage;
	
	private String authToken;
	private String pemFilePath;
	private String clientID;
	private boolean isSecureConn = false;


	/**
	 * Constructor
	 */
	public MqttClientConnector() {
		if (host != null && host.trim().length() > 0) {
			//this.sensorData = new SensorData();
			this.dataUtil = new DataUtil();
			// this.host = host;
			this.clientID = "Sample";
			logger.info("Using client id for broker connection: " + clientID);
			this.brokerAddr = protocol + "://" + host + ":" + port;
			logger.info("Using URL for broker connection: " + brokerAddr);
		}
	}
	
	public MqttClientConnector(String host, String authToken, String pemFilePath) {

		super();
		this.dataUtil = new DataUtil();
		if (host != null && host.trim().length() > 0) {
			this.host = host;
		}

		if (authToken != null && authToken.trim().length() > 0) {
			this.authToken = authToken;
		}
		if (pemFilePath != null) {
			File file = new File(pemFilePath);
			if (file.exists()) {
				this.protocol = this.config.getValue(ConfigConst.MQTT_SECTION, ConfigConst.SECURE_MQTT_PROTOCOL);
				this.port = Integer.parseInt(this.config.getValue(ConfigConst.UBIDOTS_SECTION, ConfigConst.UBIDOTS_PORT));
				this.pemFilePath = pemFilePath;
				this.isSecureConn = true;
				logger.info("PEM file valid. Using secure connection: " + pemFilePath);
			} else {
				logger.warning("PEM file invalid. Using insecure connection: " + pemFilePath);
			}
		}
		this.clientID = MqttClient.generateClientId();
		this.brokerAddr = this.protocol + "://" + this.host + ":" + this.port;
		logger.info("Using URL for broker conn: " + brokerAddr);
	}

	/**
	 * connect to mqqt broker
	 */
	public void connect() {
		if (mqttClient == null) {
			MemoryPersistence persistence = new MemoryPersistence();
			try {
				// MqqtClient object
				mqttClient = new MqttClient(brokerAddr, clientID, persistence);
				MqttConnectOptions connOpts = new MqttConnectOptions();
				connOpts.setCleanSession(true);
				if (authToken != null)
					connOpts.setUserName(authToken);
				if (isSecureConn)
					initSecureConnection(connOpts);
				logger.info("Connecting to broker: " + brokerAddr);
				// setting the callback
				mqttClient.setCallback(this);
				// connecting to broker
				mqttClient.connect(connOpts);
				logger.info("connected to broker: " + brokerAddr);
			} catch (MqttException ex) {
				logger.log(Level.SEVERE, "Failed to connect to broker " + brokerAddr, ex);
			}
		}
	}

	/**
	 * Disconnect from the broker
	 */
	public void disconnect() {
		try {
			mqttClient.disconnect();
			logger.info("Disconnect from broker: " + brokerAddr);
		} catch (Exception ex) {
			logger.log(Level.SEVERE, "Failed to disconnect from broker: " + brokerAddr, ex);
		}
	}
	
	private void publish(String topic, MqttMessage message) {
		// TODO Auto-generated method stub
		try {
			logger.info("Publishing message to topic: " + topic);
			mqttClient.publish(topic, message);
			logger.info("Message Published " + message.getId());
			
		} catch (Exception ex) {
			logger.log(Level.SEVERE, "Failed to publish Mqtt message " + ex.getMessage());
		}
		
	}

	/**
	 * publishing message
	 * 
	 * @param: topic --> topic to publish message to
	 * @param: qosLevel --> setting the qos level
	 * @param: payload --. the message to be sent
	 */
	public boolean publishMessage(String topic, int qosLevel, byte[] payload) {
		boolean msgSent = false;
		try {
			MqttMessage msg = new MqttMessage(payload);
			msg.setQos(qosLevel);
			mqttClient.publish(topic, msg);
			logger.info("Published message - " + payload.toString() + " to topic " + topic);
			msgSent = true;
		} catch (Exception ex) {
			logger.log(Level.SEVERE, "Failed to publish Mqtt message " + ex.getMessage());
		}
		return msgSent;
	}

	/**
	 * Subscribe to a topic
	 * 
	 * @param: topic --> topic name to subscribe to
	 */
	public boolean subscribeToTopic(String topic, int qos) {
		boolean success = false;
		try {
			mqttClient.subscribe(topic, qos);
			success = true;
			logger.info("Subscribed to " + topic);
		} catch (MqttException e) {
			e.printStackTrace();
		}
		return success;
	}

	/**
	 * Unsubscribing from a topic
	 */
	public boolean unSubscibe(String topic) {
		boolean success = false;
		try {
			// unsubscribe call
			mqttClient.unsubscribe(topic);
			success = true;
		} catch (MqttException e) {
			e.printStackTrace();
		}
		return success;
	}

	/**
	 * Callbacks
	 */
	@Override
	public void connectionLost(Throwable cause) {
		logger.log(Level.WARNING, "Connection to broker lost. Will retry soon.", cause);
	}

	/**
	 * called when message arrives from publisher
	 */
	@Override
	public void messageArrived(String topic, MqttMessage message) throws Exception {
		this.mqttMessage = message;
		logger.info("Message arrived: " + topic + message.getId() + "\nmessage: " + message);
		String msg = new String(message.getPayload(), StandardCharsets.UTF_8);
		int data = Integer.parseInt(msg);
		String command = data == 22 ? "DEC-TEMP" : "INC-TEMP";
		ActuatorData actuatorData = new ActuatorData("TempActuator", command, data);
		MqttClientConnector localMqtt=new MqttClientConnector();
		localMqtt.connect();
		localMqtt.publishActuatorData(actuatorData);
		/*
		 * SensorData senmsg = dataUtil.JsonToSensorData(message.toString());
		 * logger.info("SensorData Message: \n" + senmsg); logger.info ("Json message: "
		 * + dataUtil.SensorDataToJson(senmsg));
		 */
	}

	/**
	 * called when there is successfull messgage published
	 */
	@Override
	public void deliveryComplete(IMqttDeliveryToken token) {
		logger.info("Delivery Complete: " + token.getMessageId() + "-" + token.getResponse());
	}

	public MqttMessage getMessage() {
		return this.mqttMessage;
	}

	public void publishActuatorData(ActuatorData actuatorData) {
		String topic = "Actuator";
		String jsonString = this.dataUtil.toJsonFromActuatorData(actuatorData);
		logger.info("Published the following from gateway:\n" + jsonString);
		byte[] payload = jsonString.getBytes();
		this.publishMessage(topic, 2, payload);
	}
	

	public void publishTempSensorData(float sensorData) {
		String topic = "/v1.6/devices/SensePi";
		String jsonString = this.dataUtil.toUbidotsJsonFromSensorData(sensorData);
		logger.info("Published the following from gateway:\n" + jsonString);
		byte[] payload = jsonString.getBytes();
		this.publishMessage(topic, 1, payload);
	}
	
	private KeyStore readCertificate()
			throws KeyStoreException, NoSuchAlgorithmException, CertificateException, IOException {
		KeyStore ks = KeyStore.getInstance(KeyStore.getDefaultType());
		FileInputStream fis = new FileInputStream(pemFilePath);
		BufferedInputStream bis = new BufferedInputStream(fis);
		CertificateFactory cf = CertificateFactory.getInstance("X.509");
		ks.load(null);
		while (bis.available() > 0) {
			Certificate cert = cf.generateCertificate(bis);
			ks.setCertificateEntry("adk_store" + bis.available(), cert);
		}
		return ks;
	}
	
	private void initSecureConnection(MqttConnectOptions connOpts) {
		try {
			logger.info("Configuring TLS...");
			SSLContext sslContext = SSLContext.getInstance("SSL");
			KeyStore keyStore = readCertificate();
			TrustManagerFactory trustManagerFactory = TrustManagerFactory
					.getInstance(TrustManagerFactory.getDefaultAlgorithm());
			trustManagerFactory.init(keyStore);
			sslContext.init(null, trustManagerFactory.getTrustManagers(), new SecureRandom());
			connOpts.setSocketFactory(sslContext.getSocketFactory());
		} catch (Exception e) {
			logger.log(Level.SEVERE, "Failed to initialize secure MQTT connection.", e);
		}
	}



}
