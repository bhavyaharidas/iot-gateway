package neu.bhavyaharidas.connecteddevices.labs.module01;

import com.labbenchstudios.iot.common.BaseDeviceApp;
import com.labbenchstudios.iot.common.DeviceApplicationException;


public class GatewayHanlderApp extends BaseDeviceApp {
	
	public GatewayHanlderApp() {
		super();
	}

	public GatewayHanlderApp(String name, String[] args) {
		
	}
	
	public static void main(String[] args) {
		GatewayHanlderApp app = new GatewayHanlderApp(GatewayHanlderApp.class.getSimpleName(), args);
		try {
			app.start();
		} catch (DeviceApplicationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	protected void start() throws DeviceApplicationException {
		SystemPerformanceAdapter adapter = new SystemPerformanceAdapter(3);
		adapter.startPolling();
		
	}

	@Override
	protected void stop() throws DeviceApplicationException {
		// TODO Auto-generated method stub
		
	}

}
