package neu.bhavyaharidas.connecteddevices.labs.module01.Utils;

import java.lang.management.ManagementFactory;
import java.text.DecimalFormat;
import java.util.logging.ConsoleHandler;
import java.util.logging.Logger;

import com.sun.management.OperatingSystemMXBean;

import neu.bhavyaharidas.connecteddevices.labs.module01.CustomFormatter;

public class SystemCpuUtilTask {
	//static variables
	private static final Logger _Logger = Logger.getLogger(SystemCpuUtilTask.class.getSimpleName());
	private static DecimalFormat df = new DecimalFormat("0.00");

	public SystemCpuUtilTask(String string, long _pollCycle) {
	}
	
	public static void addLogFormatter() {
		CustomFormatter formatter = new CustomFormatter();
		_Logger.setUseParentHandlers(false);
		ConsoleHandler handler = new ConsoleHandler();
	    handler.setFormatter(formatter);
	    _Logger.addHandler(handler);
	}

	/**
	 * Prints and returns CPU utilization
	 */
	public double getDataFromSensor() {
		OperatingSystemMXBean bean = (OperatingSystemMXBean) ManagementFactory.getOperatingSystemMXBean();
		double res = (double) bean.getSystemCpuLoad() * 100;
		_Logger.info("CPU Utilization=" + df.format(res));
		return res;
	}
}
