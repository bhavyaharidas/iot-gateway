package neu.bhavyaharidas.connecteddevices.labs.module01;

import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import neu.bhavyaharidas.connecteddevices.labs.module01.Utils.SystemCpuUtilTask;
import neu.bhavyaharidas.connecteddevices.labs.module01.Utils.SystemMemUtilTask;

/**
 * @author Bhavya Haridas
 *
 * Class extends Thread and represents the steps that retrieve syste performance measures
 */
public class SystemPerformanceAdapter extends Thread {
	
	private static final Logger _Logger = Logger.getLogger(GatewayHanlderApp.class.getSimpleName());
	private long _pollCycle;
	private DevicePollingManager _pollManager;
	
	public SystemPerformanceAdapter(long pollCycle){
		super();
		if (pollCycle >= 1L) {
			_pollCycle = pollCycle;
		}
		_pollManager = new DevicePollingManager(2);
	}

	/**
	 * Calls CPU and Memory measurement methods in loop for 10 times at an interval of 3 seconds
	 */
	public void startPolling() {
		int i = 0;
		SystemCpuUtilTask.addLogFormatter();
		SystemMemUtilTask.addLogFormatter();
		while(i < 10) {
			//_Logger.info("Creating and scheduling CPU Utilization poller...");
			_pollManager.schedulePollingTask(new SystemCpuUtilTask("CPU Utilization", _pollCycle), _pollCycle);
			//_Logger.info("Creating and scheduling Memory Utilization poller...");
			_pollManager.schedulePollingTask(new SystemMemUtilTask("Memory Utilization", _pollCycle), _pollCycle);
			i++;
			try {
				TimeUnit.SECONDS.sleep(_pollCycle);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
