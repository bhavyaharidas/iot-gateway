package neu.bhavyaharidas.connecteddevices.labs.module01;

import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.SimpleFormatter;

/**
 * @author Bhavya Haridas
 *
 * Class extends SimpleFormatter and represents a custom formatter to be used by logger
 */
public class CustomFormatter extends SimpleFormatter {
	
	/**
	 * Overrrides format method of simple formatter
	 */
	public String format(LogRecord record){
		//System.out.println("Inside Formatter");
		  if(record.getLevel() == Level.INFO){
			  //System.out.println(record.getInstant() + " " +  record.getMessage());
		    return (record.getInstant() + " " + record.getLevel() + " " +  record.getMessage() + "\r\n");
		  }else{
		    return super.format(record);
		  }
		}
}
