package neu.bhavyaharidas.connecteddevices.labs.module01.Utils;

import java.lang.management.ManagementFactory;
import java.lang.management.MemoryMXBean;
import java.lang.management.MemoryUsage;
import java.text.DecimalFormat;
import java.util.logging.ConsoleHandler;
import java.util.logging.Logger;

import neu.bhavyaharidas.connecteddevices.labs.module01.CustomFormatter;


public class SystemMemUtilTask {

	private static final Logger _Logger = Logger.getLogger(SystemMemUtilTask.class.getSimpleName());
	private static DecimalFormat df = new DecimalFormat("0.00");
	public SystemMemUtilTask(String string, long _pollCycle) {
		// TODO Auto-generated constructor stub
	}
	
	public static void addLogFormatter() {
		CustomFormatter formatter = new CustomFormatter();
		_Logger.setUseParentHandlers(false);
		ConsoleHandler handler = new ConsoleHandler();
	    handler.setFormatter(formatter);
	    _Logger.addHandler(handler);
	}
	
	/**
	 * Prints and returns heap memory utilization
	 */
	public float getDataFromSensor() {
		MemoryMXBean memBean = ManagementFactory.getMemoryMXBean();
		MemoryUsage heap = memBean.getHeapMemoryUsage();
		MemoryUsage nonHeap = memBean.getNonHeapMemoryUsage();
		MemoryUsage memUsage = ManagementFactory.getMemoryMXBean().getHeapMemoryUsage();
		
		double heapUtil = ((double)heap.getUsed() / heap.getMax()) * 100d;
		double nonHeapUtil = ((double)nonHeap.getUsed() / nonHeap.getMax()) * 100;
		
		if(heapUtil < 0.0d) {
			heapUtil = 0.0d;
		}
		if(nonHeapUtil < 0.0d) {
			nonHeapUtil = 0.0d;
		}
		
		//_Logger.info("\tHeap Max:    " + heap.getMax() + "\tHeap Used:    " + heap.getUsed());
		//_Logger.info("\tNon-Heap Max:    " + nonHeap.getMax() + "\tNon-Heap Used:    " + nonHeap.getUsed());
		_Logger.info("Memory Utilization=" + df.format(heapUtil));
		return (float)heapUtil;
	}
}
