package neu.bhavyaharidas.connecteddevices.labs.module01;

import neu.bhavyaharidas.connecteddevices.labs.module01.Utils.SystemCpuUtilTask;

import neu.bhavyaharidas.connecteddevices.labs.module01.Utils.SystemMemUtilTask;

/**
 * @author Bhavya Haridas
 *
 * Class represents polling manager
 */
public class DevicePollingManager {

	public DevicePollingManager(int i) {
		
	}

	/**
	 * Calls method that retrieves CPU utilization
	 * Input param - SystemCpuUtilTask systemCpuUtilTask, long _pollCycle
	 */
	public void schedulePollingTask(SystemCpuUtilTask systemCpuUtilTask, long _pollCycle) {
		systemCpuUtilTask.getDataFromSensor();
	}

	/**
	 * Calls method that retrieves Memory utilization
	 * Input param - SystemMemUtilTask systemMemUtilTask, long _pollCycle
	 */
	public void schedulePollingTask(SystemMemUtilTask systemMemUtilTask, long _pollCycle) {
			systemMemUtilTask.getDataFromSensor();		
	}

}
