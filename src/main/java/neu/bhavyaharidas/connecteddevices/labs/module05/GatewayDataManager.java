package neu.bhavyaharidas.connecteddevices.labs.module05;

import neu.bhavyaharidas.connecteddevices.common.ActuatorData;
import neu.bhavyaharidas.connecteddevices.common.ActuatorDataListener;
import neu.bhavyaharidas.connecteddevices.common.ConfigConst;
import neu.bhavyaharidas.connecteddevices.common.ConfigUtil;
import neu.bhavyaharidas.connecteddevices.common.PersistenceUtil;
import neu.bhavyaharidas.connecteddevices.common.SensorData;
import neu.bhavyaharidas.connecteddevices.common.SensorDataListener;

/**
 * Class Represents data manager for gateway
 *  @author Bhavya Haridas
 *
 */
public class GatewayDataManager {
	SensorDataListener sensorDataListener;
	static PersistenceUtil persistenceUtil;
	SensorData sensorData;
	
	private ConfigUtil config = ConfigUtil.getInstance("../iot-gateway/config/ConnectedDevicesConfig.props");
	private int nominalTemp =  Integer.parseInt(this.config.getValue(ConfigConst.DEVICE_SECTION, ConfigConst.NOMINAL_DEVICE_TEMP));
	private int nominalHumid = 15;
	static int iterations;
	
	//Constructor
	public GatewayDataManager(int iterations) {
		this.iterations = iterations;
		this.persistenceUtil = new PersistenceUtil();
		for(int i = 0; i < this.iterations; i++) {
			this.readData();
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Reads sensor data from redis
	 * @param iterations
	 */
	public void readData() {
		sensorDataListener = new SensorDataListener();
		if(sensorDataListener.onMessage()) {
			sensorData = persistenceUtil.readSensorDataFromDbms();
			if(sensorData != null)	
				this.handleSensorData(sensorData);
		}
	}
	
	/**
	 * Processes the incoming sensordata object
	 * @param sensorData
	 */
	public void handleSensorData(SensorData sensorData) {
		ActuatorData actuatorData;
		if(sensorData.getName().equals("Temperature"))
			actuatorData = this.handleTemperatureData(sensorData);
        else
        	actuatorData = this.handleHumidityData(sensorData);
		this.persistenceUtil.writeActuatorDataToDbms(actuatorData);
	}
   
	/**
	 * Processes the incoming humidity sensordata object
	 * @param sensorData
	 */
    public ActuatorData handleHumidityData(SensorData sensorData) {
    	ActuatorData actuatorData;
    	float diff = sensorData.getCurValue() - this.nominalHumid;
    	String command = diff < 0 ? "INC-HMD" : "DEC-HMD";
    	String emailCommand = diff < 0 ? "below" : "above";
    	if(sensorData.getName().equals("Humidity")){
    		actuatorData = new ActuatorData(sensorData.getName(), command, diff);
    		return actuatorData;
    	}	
    	else if (sensorData.getName().equals("Humidity_I2C")) {
    		actuatorData = new ActuatorData(sensorData.getName(), command + "-I2C", diff);
    		return actuatorData;
    	}
    	return null;
    }
    
    /**
	 * Processes the incoming temperature sensordata object
	 * @param sensorData
	 */
    public ActuatorData handleTemperatureData(SensorData sensorData) {
    	ActuatorData actuatorData;
    	 if(sensorData.getCurValue() > this.nominalTemp) {
             float diff = sensorData.getCurValue() - this.nominalTemp;
             actuatorData = new ActuatorData(sensorData.getName(), "DEC-TEMP", diff);
             return actuatorData;
    	 }
    	 else if(sensorData.getCurValue() < this.nominalTemp) {
             float diff = this.nominalTemp - sensorData.getCurValue();
             actuatorData = new ActuatorData(sensorData.getName(), "INC-TEMP", diff);
             return actuatorData;
    	 }
         else
             return null;
    }
        

       
}
