package neu.bhavyaharidas.connecteddevices.labs.module02;

import com.labbenchstudios.iot.common.BaseDeviceApp;
import com.labbenchstudios.iot.common.DeviceApplicationException;

import neu.bhavyaharidas.connecteddevices.labs.module01.SystemPerformanceAdapter;


/***
 * 
 * @author Bhavya Haridas
 * Main class trigerring gateway application
 *
 */
public class GatewayHandlerApp extends BaseDeviceApp {
	
	//Constructors
	
	public GatewayHandlerApp() {
		super();
	}

	public GatewayHandlerApp(String name, String[] args) {
		
	}
	
	//Main method calling start method
	public static void main(String[] args) {
		GatewayHandlerApp app = new GatewayHandlerApp(GatewayHandlerApp.class.getSimpleName(), args);
		try {
			app.start();
		} catch (DeviceApplicationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/***
	 * Description - initializes TempEmulatorAdapter class
	 */
	@Override
	protected void start() throws DeviceApplicationException {
		TempEmulatorAdapter adapter = new TempEmulatorAdapter();
	}

	@Override
	protected void stop() throws DeviceApplicationException {
		// TODO Auto-generated method stub
		
	}

}
