package neu.bhavyaharidas.connecteddevices.labs.module02;

import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;

public class SmtpAuthenticator extends Authenticator {
	
	protected PasswordAuthentication getPasswordAuthentication(String fromEmail, String password) {
		return new PasswordAuthentication(fromEmail, password);
	}
}
