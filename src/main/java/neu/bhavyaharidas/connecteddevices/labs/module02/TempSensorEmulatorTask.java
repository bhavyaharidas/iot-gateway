package neu.bhavyaharidas.connecteddevices.labs.module02;

import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import neu.bhavyaharidas.connecteddevices.common.SensorData;

/**
 * Class extends Thread class and constantly polls SensorData every 20 seconds
 * @author Bhavya Haridas
 *
 */
public class TempSensorEmulatorTask extends Thread {
	
	//static variables
	private static final Logger _Logger = Logger.getLogger(TempSensorEmulatorTask.class.getSimpleName());
	SmtpClientConnector smtpConnector;
	static int pollingInterval;
	static SensorData sensorData;
	static float minTemp = 0;
	static float maxTemp = 30;
	
	//Constructor
	public TempSensorEmulatorTask(int pollingInterval) {
		smtpConnector = new SmtpClientConnector();
		this.pollingInterval = pollingInterval;
		sensorData = new SensorData("Lab 2");
	}
	
	/**
	 * Description - polls SensorData every 20 seconds
	 */
	public void run() {
		int i = 0;
		while(i < 20) {
			getSensorData();
			if(sensorData.getCurValue() > sensorData.getAvgValue()) {
				System.out.print("\n Current temp exceeds average by > " + getDiff() + ". Triggering alert...");
				sendNotification();
			}
			try {
				TimeUnit.SECONDS.sleep(this.pollingInterval);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			i++;
		}
	}
	
	/**
	 * Description - Calculates difference between current value and average value of sensordata
	 * @return diff value
	 */
	private float getDiff() {
		return sensorData.getCurValue() - sensorData.getAvgValue();
	}
	
	/**
	 * Description - Calls the publishMessage() method of smpt connector passing subject line and 
	 * sensordata object in byte array
	 */
	private void sendNotification()
	{
		try {		
			String subject = "Temperature above threshold";
			this.smtpConnector.publishMessage(subject, sensorData.getBytes());
		} catch (Exception e) {
			_Logger.log(Level.WARNING, "Failed to send SMTP message.", e);
		}
	}
	
	/**
	 * Description - Generates a random number representing current temperature and passes on to sensordata object
	 * @return sensordata object
	 */
	public SensorData getSensorData() {
		//Temperature value in float between 0 and 30
		float currentTemp = (float) ((Math.random() * ((maxTemp - minTemp) + 1)) + minTemp);
		sensorData.addValue(currentTemp);
		return sensorData;
	}
}
