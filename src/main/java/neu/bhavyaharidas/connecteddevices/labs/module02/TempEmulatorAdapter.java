package neu.bhavyaharidas.connecteddevices.labs.module02;

/**
 * Class represents adapter of thread to main class
 * @author Bhavya Haridas
 *
 */
public class TempEmulatorAdapter {
	
	TempSensorEmulatorTask task;

	//Constructor
	public TempEmulatorAdapter() {
		task = new TempSensorEmulatorTask(20);
		task.run();
	}
}
