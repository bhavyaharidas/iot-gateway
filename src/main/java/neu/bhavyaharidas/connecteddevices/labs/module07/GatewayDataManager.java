package neu.bhavyaharidas.connecteddevices.labs.module07;


import neu.bhavyaharidas.connecteddevices.common.ActuatorData;
import neu.bhavyaharidas.connecteddevices.common.ConfigConst;
import neu.bhavyaharidas.connecteddevices.common.ConfigUtil;
import neu.bhavyaharidas.connecteddevices.common.DataUtil;
import neu.bhavyaharidas.connecteddevices.common.SensorData;


/**
 * Class Represents data manager for gateway application.
 * Instantiates mqtt connector and listens for sensordata in a loop.
 *  @author Bhavya Haridas
 *
 */
public class GatewayDataManager {
	DataUtil dataUtil = new DataUtil();
	
	 // private var's
	private CoapServerConnector _coapServer;
	private CoapClientConnector _coapClient;
	private ConfigUtil config = ConfigUtil.getInstance("../iot-gateway/config/ConnectedDevicesConfig.props");
	private int nominalTemp =  Integer.parseInt(this.config.getValue(ConfigConst.DEVICE_SECTION, ConfigConst.NOMINAL_DEVICE_TEMP));
	private int nominalHumid = 15;
	static int iterations;
	private String host = config.getValue(ConfigConst.COAP_SECTION, ConfigConst.DEFAULT_COAP_SERVER);
	
	
	//Constructor
	public GatewayDataManager() {
		 _coapServer = new CoapServerConnector();
		 TempResourceHandler tempHandler = new TempResourceHandler(this);
		 HumidityResourceHandler humidHandler = new HumidityResourceHandler(this);
		 _coapServer.addResource(tempHandler);
		 _coapServer.addResource(humidHandler);
		 _coapServer.start();
	}
	
	/**
	 * Processes the incoming sensordata object
	 * @param sensorData
	 */
	public ActuatorData handleSensorData(SensorData sensorData) {
		ActuatorData actuatorData;
		if(sensorData.getName().equals("Temperature"))
			actuatorData = this.handleTemperatureData(sensorData);
        else
        	actuatorData = this.handleHumidityData(sensorData);
		return actuatorData;
	}
   
	/**
	 * Processes the incoming humidity sensordata object
	 * @param sensorData
	 */
    public ActuatorData handleHumidityData(SensorData sensorData) {
    	ActuatorData actuatorData;
    	float diff = sensorData.getCurValue() - this.nominalHumid;
    	String command = diff < 0 ? "INC-HMD" : "DEC-HMD";
    	String emailCommand = diff < 0 ? "below" : "above";
    	if(sensorData.getName().equals("Humidity")){
    		actuatorData = new ActuatorData(sensorData.getName(), command, diff);
    		return actuatorData;
    	}	
    	else if (sensorData.getName().equals("Humidity_I2C")) {
    		actuatorData = new ActuatorData(sensorData.getName(), command + "-I2C", diff);
    		return actuatorData;
    	}
    	return null;
    }
    
    /**
	 * Processes the incoming temperature sensordata object
	 * @param sensorData
	 */
    public ActuatorData handleTemperatureData(SensorData sensorData) {
    	ActuatorData actuatorData;
    	 if(sensorData.getCurValue() > this.nominalTemp) {
             float diff = sensorData.getCurValue() - this.nominalTemp;
             actuatorData = new ActuatorData(sensorData.getName(), "DEC-TEMP", diff);
             return actuatorData;
    	 }
    	 else if(sensorData.getCurValue() < this.nominalTemp) {
             float diff = this.nominalTemp - sensorData.getCurValue();
             actuatorData = new ActuatorData(sensorData.getName(), "INC-TEMP", diff);
             return actuatorData;
    	 }
         else
             return null;
    }
        
       
}
