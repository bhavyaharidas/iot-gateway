package neu.bhavyaharidas.connecteddevices.labs.module07;

import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.eclipse.californium.core.CoapClient;
import org.eclipse.californium.core.CoapResponse;
import org.eclipse.californium.core.WebLink;
import org.eclipse.californium.core.coap.MediaTypeRegistry;

import neu.bhavyaharidas.connecteddevices.common.ConfigConst;

public class CoapClientConnector {
	
	private static final Logger _Logger = Logger.getLogger(CoapClientConnector.class.getName());
	private String _protocol;
	private String _port;
	private String _host;
	private String _serverAddr;
	private CoapClient _clientConn;
	private boolean useNON;
	private boolean _isInitialized = false;
	
	public CoapClientConnector(String host, boolean isSecure) {
		super();
		if (isSecure) {
			_protocol = ConfigConst.SECURE_COAP_PROTOCOL;
			_port = ConfigConst.SECURE_COAP_PORT;
		} else {
		_protocol = ConfigConst.DEFAULT_COAP_PROTOCOL;
		_port = ConfigConst.DEFAULT_COAP_PORT;
		}
		if (host != null && host.trim().length() > 0) {
			_host = host;
		} else {
			_host = ConfigConst.DEFAULT_COAP_SERVER;
		}
		// NOTE: URL does not have a protocol handler for "coap",
		// so we need to construct the URL manually
		_serverAddr = _protocol + "://" + _host + ":" + _port;
		_Logger.info("Using URL for server conn: " + _serverAddr);
	}
	
	// public methods
	public void discoverResources() {
		_Logger.info("Issuing discover...");
		initClient();
		Set<WebLink> wlSet = _clientConn.discover();
		if (wlSet != null) {
			for (WebLink wl : wlSet) {
				_Logger.info(" --> WebLink: " + wl.getURI());
			}
		}
	}

	
	private void initClient(){
		initClient(null);
	}
	
	private void initClient(String resourceName){
		if (_isInitialized) {
			return;
		}
	
		if (_clientConn != null) {
			_clientConn.shutdown();
			_clientConn = null;
		}
		try {
			if (resourceName != null) {
				_serverAddr += "/" + resourceName;
			}
			_clientConn = new CoapClient(_serverAddr);
			_Logger.info("Created client connection to server / resource: " + _serverAddr);
			_isInitialized = true;
		} catch (Exception e) {
			_Logger.log(Level.SEVERE, "Failed to connect to broker: " + getCurrentUri(), e);
		}
	}
	
	/**
	 * Returns the CoAP client URI (if set, otherwise returns the serverAddr, or
	 * null).
	 *
	 * @return String
	 */
	public String getCurrentUri() {
		return (this._clientConn != null ? this._clientConn.getURI() : this._serverAddr);
	}
	
	
	public void sendGetRequest(){
		initClient();
		if (useNON) {
			_clientConn.useNONs();
		}
		_Logger.info("Sending Get request...");
		CoapResponse response = null;
		response = _clientConn.get();
		if (response != null) {
			_Logger.info(
					"Response: " + response.isSuccess() + " - " + response.getOptions() + " - " + response.getCode());
		} else {
			_Logger.warning("No response received.");
		}
	}
	
	/**
	 * send DELETE request
	 */
	public void sendDeleteRequest() {
		_Logger.info("Sending Delete request...");
		CoapResponse response = null;
		response = _clientConn.delete();
		if (response != null) {
			_Logger.info(
					"Response: " + response.isSuccess() + " - " + response.getOptions() + " - " + response.getCode());
		} else {
			_Logger.warning("No response received.");
		}
	}
	
	/**
	 * send POST request
	 * 
	 * @param payload --> the message to be sent as POST request
	 */
	public void sendPostRequest(String payload) {
		_Logger.info("Sending Post request...");
		CoapResponse response = null;
		_Logger.info("Sensor Data: " + payload);
		response = _clientConn.post(payload, MediaTypeRegistry.TEXT_PLAIN);
		if (response != null) {
			_Logger.info("Response: " + response.isSuccess() + " - " + response.getOptions() + " - " + response.getCode());
		} else {
			_Logger.warning("No response received.");
		}
	}
	

	/**
	 * send PUT request
	 * 
	 * @param payload --> the message to be sent as POST request
	 */
	public void sendPutRequest(String payload) {
		_Logger.info("Sending Put request...");
		CoapResponse response = null;
		_Logger.info("Sensor Data: " + payload);
		response = _clientConn.put(payload, MediaTypeRegistry.TEXT_PLAIN);
		if (response != null) {
			_Logger.info(
					"Response: " + response.isSuccess() + " - " + response.getOptions() + " - " + response.getCode());
		} else {
			_Logger.warning("No response received.");
		}
	}
	
	/**
	 * ping the server
	 */
	public void pingServer() {
		_Logger.info("Sending ping...");
		_clientConn.ping();
	}
}

