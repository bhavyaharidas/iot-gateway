package neu.bhavyaharidas.connecteddevices.labs.module07;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.util.logging.Logger;

import org.eclipse.californium.core.CoapResource;
import org.eclipse.californium.core.CoapServer;
import org.eclipse.californium.core.network.CoapEndpoint;
import org.eclipse.californium.core.network.EndpointManager;
import org.eclipse.californium.core.server.resources.Resource;

import neu.bhavyaharidas.connecteddevices.common.ConfigConst;
import neu.bhavyaharidas.connecteddevices.common.ConfigUtil;

/**
 * Class Represents Coap Server Connector
 * Instantiates coap server connector and listens fto requests
 *  @author Bhavya Haridas
 *
 */
public class CoapServerConnector {
	// static
	private static final Logger _Logger = Logger.getLogger(CoapServerConnector.class.getName());
	private ConfigUtil config = ConfigUtil.getInstance("../iot-gateway/config/ConnectedDevicesConfig.props");
	private int port;
	
	// private var's
	private CoapServer _coapServer;
	 
	// constructors
	public CoapServerConnector(){
		super();
		_coapServer = new CoapServer();
		this.port = Integer.parseInt(this.config.getValue(ConfigConst.COAP_SECTION, ConfigConst.DEFAULT_COAP_PORT));
		addEndpoints();
	}
	 
	// public methods
	/**
	 * Add a coap resource to the server
	 * @param resource
	 */
	public void addResource(CoapResource resource){
		if (resource != null){
			_coapServer.add(resource);
		}
	 }
	 
	/**
	 * Start the coap server
	 */
	public void start(){
		_Logger.info("Starting CoAP server...");
		_coapServer.start();
	 }
	
	public void stop(){
		_Logger.info("Stopping CoAP server...");
		_coapServer.stop();
	}

	private void addEndpoints() {
		for (InetAddress iAddr : EndpointManager.getEndpointManager().getNetworkInterfaces()) {
			if (iAddr.isLoopbackAddress()) {
				InetSocketAddress bind2Addr = new InetSocketAddress(iAddr, port);
				_coapServer.addEndpoint(new CoapEndpoint(bind2Addr));
			}
		}

	}
}
