package neu.bhavyaharidas.connecteddevices.labs.module07;

import java.util.logging.Logger;

import org.eclipse.californium.core.CoapResource;
import org.eclipse.californium.core.coap.CoAP.ResponseCode;
import org.eclipse.californium.core.server.resources.CoapExchange;

import neu.bhavyaharidas.connecteddevices.common.ActuatorData;
import neu.bhavyaharidas.connecteddevices.common.DataUtil;
import neu.bhavyaharidas.connecteddevices.common.SensorData;

/**
 * Class Represents resource handler for Temperature
 *  @author Bhavya Haridas
 *
 */
public class TempResourceHandler extends CoapResource {
	
	//private vars
	private static final Logger _Logger = Logger.getLogger(TempResourceHandler.class.getName());
	private SensorData sensorData;
	private ActuatorData actuatorData;
	public void setSensorData(SensorData sensorData) {
		this.sensorData = sensorData;
	}

	private DataUtil dataUtil = new DataUtil();
	private GatewayDataManager gatewayDataManager;
			 // constructors
	public TempResourceHandler(GatewayDataManager gatewayDataManager){
		super("Temp", true);
		this.gatewayDataManager = gatewayDataManager;
	}
	
	/* 
	 * responds to GET request
	 */
	@Override
	public void handleGET(CoapExchange ce){
		if(this.actuatorData != null)
			ce.respond(ResponseCode.VALID, this.dataUtil.toJsonFromActuatorData(this.actuatorData));
		else
			ce.respond(ResponseCode.VALID, "No actuator data");
		_Logger.info("Received GET request from client.");
	}
	
	/* 
	 * responds to POST request
	 */
	@Override
	public void handlePOST(CoapExchange ce) {
		ce.respond(ResponseCode.CREATED,"POST worked !");
		_Logger.info("Received POST request from client.");
		String recievedJson = new String(ce.getRequestPayload());
		_Logger.info("Recieved \n" + recievedJson);
		sensorData = dataUtil.toSensorDataFromJson(recievedJson);
		this.actuatorData = this.gatewayDataManager.handleSensorData(this.sensorData);
	}
	
	/*
	 * responds to PUT request 
	 */
	@Override
	public void handlePUT(CoapExchange ce) {
		ce.respond(ResponseCode.CHANGED,"PUT worked!");
		String recievedjson = new String(ce.getRequestPayload());
		_Logger.info("update: \n" + recievedjson);
		sensorData = dataUtil.toSensorDataFromJson(recievedjson);
		_Logger.info("\n" + sensorData.toString());
		_Logger.info("PUT Request was successful.\n");
	}

	/* 
	 * responds to DELETE request
	 */
	@Override
	public void handleDELETE(CoapExchange ce) {
		ce.respond(ResponseCode.DELETED,"DELETE worked!");
		_Logger.info("Received DELETE  request from client.");
	}
	
	public SensorData getSensorData() {
		return this.sensorData;
	}
}
