package neu.bhavyaharidas.connecteddevices.labs.module07;

import com.labbenchstudios.iot.common.BaseDeviceApp;
import com.labbenchstudios.iot.common.DeviceApplicationException;
import neu.bhavyaharidas.connecteddevices.labs.module07.GatewayDataManager;




/***
 * 
 * @author Bhavya Haridas
 * Main class trigerring gateway application
 *
 */
public class GatewayHandlerApp {

	public static GatewayHandlerApp _App;
	
	public static void main(String[] args){
		_App = new GatewayHandlerApp();
		try {
			_App.start();
			} 
		catch (Exception e) {
			e.printStackTrace();
		}
	 }

	 // constructors
	 
	 public GatewayHandlerApp(){
		 super();
	 }
	 
	 // public methods
	 public void start(){
		 GatewayDataManager manager = new GatewayDataManager();
	 }

}
