package neu.bhavyaharidas.connecteddevices.labs.module06;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;

import org.eclipse.paho.client.mqttv3.MqttMessage;

import neu.bhavyaharidas.connecteddevices.common.ActuatorData;
import neu.bhavyaharidas.connecteddevices.common.ActuatorDataListener;
import neu.bhavyaharidas.connecteddevices.common.ConfigConst;
import neu.bhavyaharidas.connecteddevices.common.ConfigUtil;
import neu.bhavyaharidas.connecteddevices.common.DataUtil;
import neu.bhavyaharidas.connecteddevices.common.PersistenceUtil;
import neu.bhavyaharidas.connecteddevices.common.SensorData;
import neu.bhavyaharidas.connecteddevices.common.SensorDataListener;

/**
 * Class Represents data manager for gateway application.
 * Instantiates mqtt connector and listens for sensordata in a loop.
 *  @author Bhavya Haridas
 *
 */
public class GatewayDataManager {
	DataUtil dataUtil = new DataUtil();
	
	private ConfigUtil config = ConfigUtil.getInstance("../iot-gateway/config/ConnectedDevicesConfig.props");
	private int nominalTemp =  Integer.parseInt(this.config.getValue(ConfigConst.DEVICE_SECTION, ConfigConst.NOMINAL_DEVICE_TEMP));
	private int nominalHumid = 15;
	static int iterations;
	private int previousMessageId = 0;
	
	private static MqttClientConnector mqttConnector; 
	
	//Constructor
	public GatewayDataManager() {
		mqttConnector = new MqttClientConnector();
		this.setMqttClient();
		while(true) {
			this.readData();
		}
	}
	
	/**
	 * Connects to mqtt client and subscribes to sensordata channel
	 */
	private void setMqttClient() {
		this.mqttConnector.connect();
		this.mqttConnector.subscribeToTopic("SensorData");
	}

	/**
	 * Reads sensor data read by mqtt client that is subscribed to sesnordata channel
	 */
	public void readData() {
		MqttMessage mqttMessage = this.mqttConnector.getMessage(); //Reads the current mqtt message
		if(mqttMessage != null && mqttMessage.getId() != this.previousMessageId) {
			this.previousMessageId = mqttMessage.getId();
			String sensorDataJson = new String(mqttMessage.getPayload(), StandardCharsets.UTF_8); //convert bytes to string
			this.handleSensorData(this.dataUtil.toSensorDataFromJson(sensorDataJson));
		}
	}
	
	/**
	 * Processes the incoming sensordata object
	 * @param sensorData
	 */
	public void handleSensorData(SensorData sensorData) {
		ActuatorData actuatorData;
		if(sensorData.getName().equals("Temperature"))
			actuatorData = this.handleTemperatureData(sensorData);
        else
        	actuatorData = this.handleHumidityData(sensorData);
		this.mqttConnector.publishActuatorData(actuatorData);
	}
   
	/**
	 * Processes the incoming humidity sensordata object
	 * @param sensorData
	 */
    public ActuatorData handleHumidityData(SensorData sensorData) {
    	ActuatorData actuatorData;
    	float diff = sensorData.getCurValue() - this.nominalHumid;
    	String command = diff < 0 ? "INC-HMD" : "DEC-HMD";
    	String emailCommand = diff < 0 ? "below" : "above";
    	if(sensorData.getName().equals("Humidity")){
    		actuatorData = new ActuatorData(sensorData.getName(), command, diff);
    		return actuatorData;
    	}	
    	else if (sensorData.getName().equals("Humidity_I2C")) {
    		actuatorData = new ActuatorData(sensorData.getName(), command + "-I2C", diff);
    		return actuatorData;
    	}
    	return null;
    }
    
    /**
	 * Processes the incoming temperature sensordata object
	 * @param sensorData
	 */
    public ActuatorData handleTemperatureData(SensorData sensorData) {
    	ActuatorData actuatorData;
    	 if(sensorData.getCurValue() > this.nominalTemp) {
             float diff = sensorData.getCurValue() - this.nominalTemp;
             actuatorData = new ActuatorData(sensorData.getName(), "DEC-TEMP", diff);
             return actuatorData;
    	 }
    	 else if(sensorData.getCurValue() < this.nominalTemp) {
             float diff = this.nominalTemp - sensorData.getCurValue();
             actuatorData = new ActuatorData(sensorData.getName(), "INC-TEMP", diff);
             return actuatorData;
    	 }
         else
             return null;
    }
        

       
}
