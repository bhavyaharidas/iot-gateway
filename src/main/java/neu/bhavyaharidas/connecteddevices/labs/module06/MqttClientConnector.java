package neu.bhavyaharidas.connecteddevices.labs.module06;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

import neu.bhavyaharidas.connecteddevices.common.ActuatorData;
import neu.bhavyaharidas.connecteddevices.common.ConfigConst;
import neu.bhavyaharidas.connecteddevices.common.DataUtil;
import neu.bhavyaharidas.connecteddevices.common.SensorData;

public class MqttClientConnector implements MqttCallback {
	private static final Logger logger = Logger.getLogger(MqttClientConnector.class.getName());
	private String protocol = ConfigConst.DEFAULT_MQTT_PROTOCOL;
	private String host = ConfigConst.DEFAULT_MQTT_SERVER;
	private int port = ConfigConst.DEFAULT_MQTT_PORT;

	private String clientID;
	private String brokerAddr;
	private MqttClient mqttClient;
	private DataUtil dataUtil;
	private MqttMessage mqttMessage;


	/**
	 * Constructor
	 */
	public MqttClientConnector() {
		if (host != null && host.trim().length() > 0) {
			//this.sensorData = new SensorData();
			this.dataUtil = new DataUtil();
			// this.host = host;
			this.clientID = "Sample";
			logger.info("Using client id for broker connection: " + clientID);
			this.brokerAddr = protocol + "://" + host + ":" + port;
			logger.info("Using URL for broker connection: " + brokerAddr);
		}
	}

	/**
	 * connect to mqqt broker
	 */
	public void connect() {
		if (mqttClient == null) {
			MemoryPersistence persistence = new MemoryPersistence();
			try {
				// MqqtClient object
				mqttClient = new MqttClient(brokerAddr, clientID, persistence);
				MqttConnectOptions connOpts = new MqttConnectOptions();
				connOpts.setCleanSession(true);
				
				// setting the callback
				mqttClient.setCallback(this);
				// connecting to broker
				mqttClient.connect(connOpts);
				logger.info("connected to broker: " + brokerAddr);
			} catch (MqttException ex) {
				logger.log(Level.SEVERE, "Failed to connect to broker" + brokerAddr, ex);
			}
		}
	}

	/**
	 * Disconnect from the broker
	 */
	public void disconnect() {
		try {
			mqttClient.disconnect();
			logger.info("Disconnect from broker: " + brokerAddr);
		} catch (Exception ex) {
			logger.log(Level.SEVERE, "Failed to disconnect from broker: " + brokerAddr, ex);
		}
	}

	/**
	 * publishing message
	 * 
	 * @param: topic --> topic to publish message to
	 * @param: qosLevel --> setting the qos level
	 * @param: payload --. the message to be sent
	 */
	public boolean publishMessage(String topic, int qosLevel, byte[] payload) {
		boolean msgSent = false;
		try {
			MqttMessage msg = new MqttMessage(payload);
			msg.setQos(qosLevel);
			mqttClient.publish(topic, msg);
			msgSent = true;
		} catch (Exception ex) {
			logger.log(Level.SEVERE, "Failed to publish Mqtt message " + ex.getMessage());
		}
		return msgSent;
	}

	/**
	 * Subscribe to a topic
	 * 
	 * @param: topic --> topic name to subscribe to
	 */
	public boolean subscribeToTopic(String topic) {
		boolean success = false;
		try {
			mqttClient.subscribe(topic);
			success = true;
			logger.info("Subscribed to " + topic);
		} catch (MqttException e) {
			e.printStackTrace();
		}
		return success;
	}

	/**
	 * Unsubscribing from a topic
	 */
	public boolean unSubscibe(String topic) {
		boolean success = false;
		try {
			// unsubscribe call
			mqttClient.unsubscribe(topic);
			success = true;
		} catch (MqttException e) {
			e.printStackTrace();
		}
		return success;
	}

	/**
	 * Callbacks
	 */
	@Override
	public void connectionLost(Throwable cause) {
		logger.log(Level.WARNING, "Connection to broker lost. Will retry soon.", cause);
	}

	/**
	 * called when message arrives from publisher
	 */
	@Override
	public void messageArrived(String topic, MqttMessage message) throws Exception {

		logger.info("Received the following message on Gateway:\n " + message);
		this.mqttMessage = message;
		/*
		 * SensorData senmsg = dataUtil.JsonToSensorData(message.toString());
		 * logger.info("SensorData Message: \n" + senmsg); logger.info ("Json message: "
		 * + dataUtil.SensorDataToJson(senmsg));
		 */
	}

	/**
	 * called when there is successfull messgage published
	 */
	@Override
	public void deliveryComplete(IMqttDeliveryToken token) {
		logger.info("Delivery Complete: " + token.getMessageId() + "-" + token.getResponse());
	}

	public MqttMessage getMessage() {
		return this.mqttMessage;
	}

	public void publishActuatorData(ActuatorData actuatorData) {
		String topic = "Actuator";
		String jsonString = this.dataUtil.toJsonFromActuatorData(actuatorData);
		logger.info("Published the following from gateway:\n" + jsonString);
		byte[] payload = jsonString.getBytes();
		this.publishMessage(topic, 2, payload);
	}

}
