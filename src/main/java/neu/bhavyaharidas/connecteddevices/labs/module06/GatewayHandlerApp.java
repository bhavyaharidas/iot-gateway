package neu.bhavyaharidas.connecteddevices.labs.module06;

import com.labbenchstudios.iot.common.BaseDeviceApp;
import com.labbenchstudios.iot.common.DeviceApplicationException;

import neu.bhavyaharidas.connecteddevices.common.PersistenceUtil;
import neu.bhavyaharidas.connecteddevices.common.SensorDataListener;




/***
 * 
 * @author Bhavya Haridas
 * Main class trigerring gateway application
 *
 */
public class GatewayHandlerApp extends BaseDeviceApp {
	SensorDataListener sensorDataListener;
	//Constructors
	
	public GatewayHandlerApp() {
		super();
	}

	public GatewayHandlerApp(String name, String[] args) {
		
	}
	
	//Main method calling start method
	public static void main(String[] args) {
		GatewayHandlerApp app = new GatewayHandlerApp(GatewayHandlerApp.class.getSimpleName(), args);
		try {
			app.start();
		} catch (DeviceApplicationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/***
	 * Description - initializes TempEmulatorAdapter class
	 */
	@Override
	protected void start() throws DeviceApplicationException {
		GatewayDataManager manager = new GatewayDataManager();
	}

	@Override
	protected void stop() throws DeviceApplicationException {
		// TODO Auto-generated method stub
		
	}
	
	/*
	public void activateListener() {
		this.sensorDataListener = new SensorDataListener();
		this.persistenceUtil.registerSensorDataDbmsListener(this.sensorDataListener);
	}
	*/
}
