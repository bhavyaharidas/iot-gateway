/**
 * 
 */
package neu.bhavyaharidas.connecteddevices.common;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertTrue;

/**
 * Test class for DataUtil functionality.
 * 
 * Instructions:
 * 1) Rename 'testSomething()' method such that 'Something' is specific to your needs; add others as needed, beginning each method with 'test...()'.
 * 2) Add the '@Test' annotation to each new 'test...()' method you add.
 * 3) Import the relevant modules and classes to support your tests.
 * 4) Run this class as unit test app
 * 5) Include a screen shot of the report when you submit your assignment
 * 
 * Please note: While some example test cases may be provided, you must write your own for the class.
 */
public class DataUtilTest
{
	// setup methods
	static SensorData sensorData;
	static ActuatorData actuatorData;
	static DataUtil dataUtil;
	
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception
	{
		sensorData = new SensorData("Test Lab 5");
		sensorData.addValue(14.8f);
		
		String command1 = "INCTEMP";
		String command2 = "DECTEMP";
		int testValue = 10;
		actuatorData = new ActuatorData("data1", command1, testValue);
		
		dataUtil = new DataUtil();
	}
	
	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception
	{
		sensorData = null;
		actuatorData = null;
	}
	
	// test methods
	
	/**
	 * 
	 */
	@Test
	public void testActuatorDataToJson()
	{
		String jsonData = this.dataUtil.toJsonFromActuatorData(this.actuatorData);
		ActuatorData data = this.dataUtil.toActuatorDataFromJson(jsonData);
		assertTrue(data.getName().equals(this.actuatorData.getName()));
		assertTrue(data.getCommand().equals(this.actuatorData.getCommand()));
		assertTrue(data.getValue() == this.actuatorData.getValue());
	}
	
	
	/**
	 * 
	 */
	@Test
	public void testSensorDataToJson()
	{
		String jsonData = this.dataUtil.toJsonFromSensorData(this.sensorData);
		SensorData data = this.dataUtil.toSensorDataFromJson(jsonData);
		assertTrue(data.getName().equals(this.sensorData.getName()));
		assertTrue(data.getCurValue() == this.sensorData.getCurValue());
		assertTrue(data.getAvgValue() == this.sensorData.getAvgValue());
		assertTrue(data.getMaxValue() == this.sensorData.getMaxValue());
		assertTrue(data.getMinValue() == this.sensorData.getMinValue());
		assertTrue(data.getSampleCount() == this.sensorData.getSampleCount());
	}
	
	/**
	 * 
	 */
	@Test
	public void testJsonToActuatorData()
	{
		String jsonData = this.dataUtil.toJsonFromActuatorData(this.actuatorData);
		ActuatorData data = this.dataUtil.toActuatorDataFromJson(jsonData);
		assertTrue(data.getName().equals(this.actuatorData.getName()));
		assertTrue(data.getCommand().equals(this.actuatorData.getCommand()));
		assertTrue(data.getValue() == this.actuatorData.getValue());
	}
	
	
	/**
	 * 
	 */
	@Test
	public void testJsonToSensorData()
	{
		String jsonData = this.dataUtil.toJsonFromSensorData(this.sensorData);
		SensorData data = this.dataUtil.toSensorDataFromJson(jsonData);
		assertTrue(data.getName().equals(this.sensorData.getName()));
		assertTrue(data.getCurValue() == this.sensorData.getCurValue());
		assertTrue(data.getAvgValue() == this.sensorData.getAvgValue());
		assertTrue(data.getMaxValue() == this.sensorData.getMaxValue());
		assertTrue(data.getMinValue() == this.sensorData.getMinValue());
		assertTrue(data.getSampleCount() == this.sensorData.getSampleCount());
	}
	
}
