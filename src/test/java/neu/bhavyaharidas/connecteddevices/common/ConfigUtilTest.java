/**
 * 
 */

package neu.bhavyaharidas.connecteddevices.common;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

/**
 * Test class for ConfigUtil functionality.
 * 
 * Instructions:
 * 1) Rename 'testSomething()' method such that 'Something' is specific to your needs; add others as needed, beginning each method with 'test...()'.
 * 2) Add the '@Test' annotation to each new 'test...()' method you add.
 * 3) Import the relevant modules and classes to support your tests.
 * 4) Run this class as unit test app.
 * 5) Include a screen shot of the report when you submit your assignment.
 * 
 * Please note: While some example test cases may be provided, you must write your own for the class.
 */
public class ConfigUtilTest
{
	// static
	
	public static final String DIR_PREFIX = "./sample/";
	
	public static final String TEST_VALID_CFG_FILE   = DIR_PREFIX + "ConnectedDevicesConfig.props";
	
	public static ConfigUtil configUtil;
	
	// setup methods
	
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception
	{
		configUtil = ConfigUtil.getInstance("../iot-gateway/config/test/ConnectedDevicesConfig.props");
		configUtil.loadConfig();
	}
	
	// test methods
	
	/**
	 * Test method for {@link neu.bhavyaharidas.connecteddevices.common.ConfigUtil#getValue(java.lang.String, java.lang.String)}.
	 */
	@Test
	public void testGetValue()
	{
		String section = "smtp.cloud";
		String key = "host";
		
		String value = configUtil.getValue(section, key);
		String secondValue = configUtil.getValue("abc", key);
		String thirdValue = configUtil.getValue(section, "abc");

        assertTrue("Value Found", value != null);
        assertTrue("Value Not found", secondValue == "");
        assertTrue("Value not found", thirdValue == "");
	}
	
	/**
	 * Test method for {@link neu.bhavyaharidas.connecteddevices.common.ConfigUtil#hasConfigData()}.
	 */
	@Test
	public void testHasConfigData()
	{
		assertTrue(configUtil.hasConfigData());
	}
	
	/**
	 * Test method for {@link com.labbenchstudios.iot.common.ConfigUtil#loadConfig()}.
	 */
	@Test
	public void testLoadConfig()
	{
		assertTrue(configUtil.loadConfig());
	}
	
}
