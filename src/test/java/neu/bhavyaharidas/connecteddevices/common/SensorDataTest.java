/**
 * 
 */
package neu.bhavyaharidas.connecteddevices.common;

import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * Test class for SensorData functionality.
 * 
 * Instructions:
 * 1) Rename 'testSomething()' method such that 'Something' is specific to your needs; add others as needed, beginning each method with 'test...()'.
 * 2) Add the '@Test' annotation to each new 'test...()' method you add.
 * 3) Import the relevant modules and classes to support your tests.
 * 4) Run this class as unit test app.
 * 5) Include a screen shot of the report when you submit your assignment.
 * 
 * Please note: While some example test cases may be provided, you must write your own for the class.
 */
public class SensorDataTest
{
	static SensorData sensorData;
	// setup methods
	
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception
	{
		sensorData = new SensorData("Test Lab 2");
		sensorData.addValue(14.8f);
	}
	
	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception
	{
		sensorData = null;
	}
	
	/**
	 * Test method for {@link neu.bhavyaharidas.connecteddevices.common.SensorData#addValue()}.
	 */
	@Test
	public void testAddValue()
	{
		float value1 = 25.8f;
		float value2 = -0.8f;
		float value3 = 56.98f;
		sensorData.addValue(value1);
		sensorData.addValue(value2);
		sensorData.addValue(value3);
	}
	
	/**
	 * Test method for {@link neu.bhavyaharidas.connecteddevices.common.SensorData#getAverageValue()()}.
	 */
	@Test
	public void testGetAverageValue()
	{
		assertTrue(sensorData.getAvgValue() > 0 && sensorData.getAvgValue() < 30);
	}
	
	/**
	 * Test method for {@link neu.bhavyaharidas.connecteddevices.common.SensorData#getCount()}.
	 */
	@Test
	public void testGetCount()
	{
		assertTrue(sensorData.getSampleCount() > 0);
	}
	
	/**
	 * Test method for {@link neu.bhavyaharidas.connecteddevices.common.SensorData#getCurrentValue()}.
	 */
	@Test
	public void testGetCurrentValue()
	{
		assertTrue(sensorData.getCurValue() > 0 && sensorData.getCurValue() < 30);
	}
	
	/**
	 * Test method for {@link neu.bhavyaharidas.connecteddevices.common.SensorData#getMaxValue()}.
	 */
	@Test
	public void testGetMaxValue()
	{
		assertTrue(sensorData.getMaxValue() > 0 && sensorData.getMaxValue() < 30);
	}
	
	/**
	 * Test method for {@link neu.bhavyaharidas.connecteddevices.common.SensorData#getMinValue()}.
	 */
	@Test
	public void testGetMinValue()
	{
		assertTrue(sensorData.getMinValue() > 0 && sensorData.getMinValue() < 30);
	}
	
	/**
	 * Test method for {@link neu.bhavyaharidas.connecteddevices.common.SensorData#getName()}.
	 */
	@Test
	public void testGetName()
	{
		assertTrue(sensorData.getName() instanceof String);
	}
	
	/**
	 * Test method for {@link neu.bhavyaharidas.connecteddevices.common.SensorData#setName()}.
	 */
	@Test
	public void testSetName()
	{
		sensorData.setName("Test set name");
	}
	
}
