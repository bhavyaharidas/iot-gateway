/**
 * 
 */
package neu.bhavyaharidas.connecteddevices.labs;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertTrue;
import neu.bhavyaharidas.connecteddevices.common.SensorData;
import neu.bhavyaharidas.connecteddevices.labs.module02.SmtpClientConnector;
import neu.bhavyaharidas.connecteddevices.labs.module02.TempSensorEmulatorTask;

/**
 * Test class for all requisite Module02 functionality.
 * 
 * Instructions:
 * 1) Rename 'testSomething()' method such that 'Something' is specific to your needs; add others as needed, beginning each method with 'test...()'.
 * 2) Add the '@Test' annotation to each new 'test...()' method you add.
 * 3) Import the relevant modules and classes to support your tests.
 * 4) Run this class as unit test app.
 * 5) Include a screen shot of the report when you submit your assignment.
 * 
 * Please note: While some example test cases may be provided, you must write your own for the class.
 */
public class Module02Test
{
	TempSensorEmulatorTask emulator;
	SmtpClientConnector smtpConnector;
	// setup methods
	
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception
	{
		emulator = new TempSensorEmulatorTask(10);
		smtpConnector = new SmtpClientConnector();
	}
	
	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception
	{
		emulator = null;
		smtpConnector = null;
	}
	
	// test methods
	
	/**
	 * Test method for {@link neu.bhavyaharidas.connecteddevices.labs.module02.TempSensorEmulatorTask#getSensorData()}.
	 */
	@Test
	public void testGetSensorData()
	{
		assertTrue("SensorData object returned", emulator.getSensorData() instanceof SensorData);
	}
	
	/**
	 * Test method for {@link neu.bhavyaharidas.connecteddevices.labs.module02.SmtpClientConnector#publishMessage(String subject, byte[] payload)}.
	 */
	@Test
	public void testPublishMessage()
	{
		String subject = "Test Subject";
		String testBody = "Test Message from Junit";
		
		smtpConnector.publishMessage(subject, testBody.getBytes());
	}
	
}
