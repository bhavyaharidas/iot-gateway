/**
 * 
 */
package neu.bhavyaharidas.connecteddevices.labs;

import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import neu.bhavyaharidas.connecteddevices.common.PerformanceData;
import neu.bhavyaharidas.connecteddevices.common.SensorData;
import neu.bhavyaharidas.connecteddevices.labs.module01.Utils.SystemCpuUtilTask;
import neu.bhavyaharidas.connecteddevices.labs.module01.Utils.SystemMemUtilTask;
import neu.bhavyaharidas.connecteddevices.labs.module11.SmtpClientConnector;
import neu.bhavyaharidas.connecteddevices.labs.module11.SystemPerformanceTask;
import neu.bhavyaharidas.connecteddevices.labs.module11.UbidotsAPIConnector;

/**
 * Test class for all requisite Module11 functionality.
 * 
 * Instructions:
 * 1) Rename 'testSomething()' method such that 'Something' is specific to your needs; add others as needed, beginning each method with 'test...()'.
 * 2) Add the '@Test' annotation to each new 'test...()' method you add.
 * 3) Import the relevant modules and classes to support your tests.
 * 4) Run this class as unit test app.
 * 5) Include a screen shot of the report when you submit your assignment.
 * 
 * Please note: While some example test cases may be provided, you must write your own for the class.
 */
public class Module11Test
{
	PerformanceData perfData;
	SmtpClientConnector smtpConnector;
	SystemPerformanceTask system;
	UbidotsAPIConnector apiConnector;
	
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception
	{
		perfData = new PerformanceData();
		apiConnector = new UbidotsAPIConnector();
		smtpConnector = new SmtpClientConnector();
		system = new SystemPerformanceTask(apiConnector, 10);
	}
	
	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception
	{
	}
	
	// test methods
	
	/**
	 * SMTP email connector unit test
	 */
	@Test
	public void testPublishMessage()
	{
		String subject = "Test Subject";
		String testBody = "Test Message from Junit";
		
		smtpConnector.publishMessage(subject, testBody.getBytes());
	}
	
	/**
	 * 
	 */
	@Test
	public void testSystemCpuUtilLower()
	{
		double cpuUtil = system.getCpu();
		
		int result = Double.compare(cpuUtil, 0);
        assertTrue("CPU Utilization Greater than 0", result >= 0);
	}
	
	/**
	 * 
	 */
	@Test
	public void testSystemCpuUtilUpper()
	{
		double cpuUtil = system.getCpu();
		
		int result = Double.compare(cpuUtil, 0);
        assertTrue("CPU Utilization Less than 100", result <= 100);
	}
	
	/**
	 * 
	 */
	@Test
	public void testSystemMemUtilLower()
	{
		double memUtil = system.getMemory();
		
		int result = Double.compare(memUtil, 0);
        assertTrue("Memory Utilization Greater than 0", result >= 0);
        
	}
	
	/**
	 * 
	 */
	@Test
	public void testSystemMemUtilUpper()
	{
		double memUtil = system.getMemory();
		
		int result = Double.compare(memUtil, 0);
        assertTrue("Memory Utilization Less than 100", result <= 100);
	}
	
	/**
	 * 
	 */
	@Test
	public void testUbidotsApiConnector()
	{
		float cpuUtil = system.getCpu();
		float memUtil = system.getMemory();
		
		this.apiConnector.postGatewayCpu(cpuUtil);
		this.apiConnector.postGatewayMemory(memUtil);
	}
	
}
