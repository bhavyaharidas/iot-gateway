/**
 * 
 */
package neu.bhavyaharidas.connecteddevices.labs;

import java.nio.charset.StandardCharsets;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertTrue;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import neu.bhavyaharidas.connecteddevices.labs.module06.MqttClientConnector;

/**
 * Test class for all requisite Module06 functionality.
 * 
 * Instructions:
 * 1) Rename 'testSomething()' method such that 'Something' is specific to your needs; add others as needed, beginning each method with 'test...()'.
 * 2) Add the '@Test' annotation to each new 'test...()' method you add.
 * 3) Import the relevant modules and classes to support your tests.
 * 4) Run this class as unit test app.
 * 5) Include a screen shot of the report when you submit your assignment.
 * 
 * Please note: While some example test cases may be provided, you must write your own for the class.
 */
public class Module06Test
{
	MqttClientConnector mqttConnector;
	MqttClientConnector mqttConnector2;
	// setup methods
	
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception
	{
		mqttConnector = new MqttClientConnector();
		mqttConnector.connect();
		mqttConnector.subscribeToTopic("TestTopic");
		
		mqttConnector2 = new MqttClientConnector();
		mqttConnector2.connect();
	}
	
	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception
	{
	}
	
	// test methods
	
	/**
	 * 
	 */
	@Test
	public void testPubSub()
	{
		String testData = "Test Data";
		this.mqttConnector2.publishMessage("TestTopic", 2, testData.getBytes());
		try {
			TimeUnit.SECONDS.sleep(2);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String receivedData = new String(this.mqttConnector.getMessage().getPayload());
		System.out.println("Received - " + receivedData);
		assertTrue(testData.contentEquals(receivedData));
	}
	
}
