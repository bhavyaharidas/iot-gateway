/**
 * 
 */
package neu.bhavyaharidas.connecteddevices.labs;

import static org.junit.Assert.assertTrue;

import java.util.Random;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import neu.bhavyaharidas.connecteddevices.common.ConfigConst;
import neu.bhavyaharidas.connecteddevices.common.ConfigUtil;
import neu.bhavyaharidas.connecteddevices.labs.module08.MqttClientConnector;
import neu.bhavyaharidas.connecteddevices.labs.module08.UbidotsAPIConnector;

/**
 * Test class for all requisite Module08 functionality.
 * 
 * Instructions:
 * 1) Rename 'testSomething()' method such that 'Something' is specific to your needs; add others as needed, beginning each method with 'test...()'.
 * 2) Add the '@Test' annotation to each new 'test...()' method you add.
 * 3) Import the relevant modules and classes to support your tests.
 * 4) Run this class as unit test app.
 * 5) Include a screen shot of the report when you submit your assignment.
 * 
 * Please note: While some example test cases may be provided, you must write your own for the class.
 */
public class Module08Test
{
	private ConfigUtil config = ConfigUtil.getInstance("../iot-gateway/config/ConnectedDevicesConfig.props");
	private static MqttClientConnector secureMqttConnector;
	private static UbidotsAPIConnector ubidotsAPIConnector;
	public static final String UBIDOTS_VARIABLE_LABEL = "/tempsensor";
	public static final String UBIDOTS_DEVICE_LABEL = "/SensorPi";
	public static final String UBIDOTS_TOPIC_DEFAULT = "/v1.6/devices" + UBIDOTS_DEVICE_LABEL + UBIDOTS_VARIABLE_LABEL;
	
	private String host = config.getValue(ConfigConst.UBIDOTS_SECTION, ConfigConst.DEFAULT_UBIDOTS_SERVER);
	private String pemFileName = config.getValue(ConfigConst.UBIDOTS_SECTION, ConfigConst.CERT_FILE_PATH);
	private String authToken = config.getValue(ConfigConst.UBIDOTS_SECTION, ConfigConst.UBIDOTS_AUTH_TOKEN);
	// setup methods
	
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception
	{
		ubidotsAPIConnector = new UbidotsAPIConnector();
		secureMqttConnector = new MqttClientConnector(host, authToken, pemFileName);
		secureMqttConnector.connect();
	}
	
	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception
	{
	}
	
	// test methods
	
	/**
	 * 
	 */
	@Test
	public void testPublisher()
	{
		secureMqttConnector.publishTempSensorData(generateRandomvalue(0.0f, 40.0f));
	}

	
	// test methods
	
	/**
	 * 
	 */
	@Test
	public void testAPI()
	{
		float testValue = generateRandomvalue(30.0f, 40.0f);
		this.ubidotsAPIConnector.postHumidValue(testValue);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		float humidActuatorValue = this.ubidotsAPIConnector.getHumidActuatorValue();
		assertTrue(humidActuatorValue == 25);
	}
	
	/**
	 * Ramdomly generates floating point number between 
	 * min and max value to emulate sensor value
	 * @param min: minimum value to be generated
	 * @param max: maximum value to be generated
	 */
	public float generateRandomvalue(float min, float max) {
		Random r = new Random();
		float random = min + r.nextFloat() * (max - min);
		return random;
	}
	
}
