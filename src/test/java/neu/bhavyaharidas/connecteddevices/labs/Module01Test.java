/**
 * 
 */
package neu.bhavyaharidas.connecteddevices.labs;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import neu.bhavyaharidas.connecteddevices.labs.module01.Utils.SystemCpuUtilTask;
import neu.bhavyaharidas.connecteddevices.labs.module01.Utils.SystemMemUtilTask;

/**
 * Test class for all requisite Module01 functionality.
 * 
 * Instructions:
 * 1) Rename 'testSomething()' method such that 'Something' is specific to your needs; add others as needed, beginning each method with 'test...()'.
 * 2) Add the '@Test' annotation to each new 'test...()' method you add.
 * 3) Import the relevant modules and classes to support your tests.
 * 4) Run this class as unit test app.
 * 5) Include a screen shot of the report when you submit your assignment.
 * 
 * Please note: While some example test cases may be provided, you must write your own for the class.
 */
@SuppressWarnings("deprecation")
public class Module01Test
{
	// setup methods
	
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception
	{
	}
	
	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception
	{
	}
	
	// test methods
	
	/**
	 * 
	 */
	@Test
	public void testSystemCpuUtilLower()
	{
		SystemCpuUtilTask cpuTask = new SystemCpuUtilTask("Testing cpu task", 2);
		double cpuUtil = cpuTask.getDataFromSensor();
		
		int result = Double.compare(cpuUtil, 0);
        assertTrue("CPU Utilization Greater than 0", result >= 0);
	}
	
	/**
	 * 
	 */
	@Test
	public void testSystemCpuUtilUpper()
	{
		SystemCpuUtilTask cpuTask = new SystemCpuUtilTask("Testing cpu task", 2);
		double cpuUtil = cpuTask.getDataFromSensor();
		
		int result = Double.compare(cpuUtil, 0);
        assertTrue("CPU Utilization Less than 100", result <= 100);
	}
	
	/**
	 * 
	 */
	@Test
	public void testSystemMemUtilLower()
	{
		SystemMemUtilTask memTask = new SystemMemUtilTask("Testing mem task", 2);
		double memUtil = memTask.getDataFromSensor();
		
		int result = Double.compare(memUtil, 0);
        assertTrue("Memory Utilization Greater than 0", result >= 0);
        
	}
	
	/**
	 * 
	 */
	@Test
	public void testSystemMemUtilUpper()
	{
		SystemMemUtilTask memTask = new SystemMemUtilTask("Testing mem task", 2);
		double memUtil = memTask.getDataFromSensor();
		
		int result = Double.compare(memUtil, 0);
        assertTrue("Memory Utilization Less than 100", result <= 100);
        
	}
}
